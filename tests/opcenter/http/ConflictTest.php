<?php

	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class ConflictTest extends TestFramework
	{
		const EXCEPTION_LEVEL = Error_Reporter::E_ERROR;

		public function testConflict()
		{
			$all = ['foo.com', 'bar.com', 'test.foo.com'];
			$this->assertEmpty(\Opcenter\Http\Apache::conflicts('car.com', $all));
			$this->assertSame(['foo.com'], \Opcenter\Http\Apache::conflicts('bar.foo.com', $all));
			$this->assertSame(['foo.com', 'test.foo.com'], \Opcenter\Http\Apache::conflicts('baz.test.foo.com', $all));
			$this->assertEmpty(\Opcenter\Http\Apache::conflicts('ebar.com', $all));
		}

		public function testConflictAdd()
		{
			$conflictDomain = 'conflict-domain.test';
			$domain = \Opcenter\Account\Ephemeral::create(['siteinfo.domain' => $conflictDomain]);
			$count = \Opcenter\Http\Apache::conflicts("test.$conflictDomain");
			$this->assertGreaterThan(0, $count, 'Conflict discovered');
			$domain2 = \Opcenter\Account\Ephemeral::create(['siteinfo.domain' => "test.$conflictDomain"]);
			$hash = \Opcenter\Http\Apache::hashDomain($domain2->getContext()->domain, \count($count));
			$this->assertFileExists(\Opcenter\Http\Apache::siteConfigurationPath($hash));
			$domain->destroy();
			$domain2->destroy();
		}

	}

