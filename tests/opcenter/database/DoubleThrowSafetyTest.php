<?php
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2020
 */

require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class DoubleThrowSafetyTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testMySQLDTSS()
		{
			$this->dtss('mysql', 'MySQL');
		}

		public function testPostgreSQLDTSS()
		{
			$this->dtss('pgsql', 'PostgreSQL');
		}

		private function dtss(string $module, string $class) {
			$acct = \Opcenter\Account\Ephemeral::create([
				"${module}.enabled" => 1,
			]);
			$class = "\Opcenter\Database\\${class}";
			$oldPrefix = $acct->getContext()->getAccount()->cur[$module]['dbaseprefix'];

			$this->assertTrue($acct->getApnscpFunctionInterceptor()->{$module . '_create_database'}("foo"), 'Created database');
			$this->assertTrue($class::databaseExists("${oldPrefix}foo"));
			$this->assertTrue($acct->getApnscpFunctionInterceptor()->{$module . '_database_exists'}("foo"));
			$this->assertTrue(\Util_Account_Editor::instantiateContexted($acct->getContext())->setConfig([
				"${module}.enabled" => 0
			])->edit());


			$this->assertTrue(\Opcenter\Map::load($class::PREFIX_MAP)->exists($oldPrefix));

			// check if db exists
			$this->assertTrue($acct->getApnscpFunctionInterceptor()->{$module . '_database_exists'}("foo"));
			$this->assertTrue($class::databaseExists("${oldPrefix}foo"));

			$this->assertEquals(0, $acct->getContext()->getAccount()->cur[$module]['enabled']);
			$this->assertSame($oldPrefix, $acct->getContext()->getAccount()->cur[$module]['dbaseprefix']);

			$this->assertTrue(\Util_Account_Editor::instantiateContexted($acct->getContext())->setConfig([
				"${module}.dbaseprefix" => null,
			])->edit());
			$this->assertNull($acct->getContext()->getAccount()->cur[$module]['dbaseprefix']);
			$this->assertNull($acct->getApnscpFunctionInterceptor()->common_get_service_value($module, 'dbaseprefix'));
			$this->assertFalse(\Opcenter\Map::load($class::PREFIX_MAP)->exists($oldPrefix));
			for ($i = 0; $i < 15; $i++, sleep(1)) {
				$ret = $class::databaseExists("${oldPrefix}foo");
				if (!$ret) {
					break;
				}
			}
			$this->assertFalse($ret);
		}
	}

