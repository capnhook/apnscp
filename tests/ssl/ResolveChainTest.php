<?php
require_once dirname(__FILE__) . '/../TestFramework.php';

class CertificateValid extends TestFramework
{
    const CRT_CHAIN = "assets/sha256.crt";
    const CRT_CHAIN_EXPECTED = "assets/chain-resolve.crt";
	private $fsRoot;
	private $sourceDir;
	protected $auth;

	public function setUp()
	{
		parent::setUp(); // TODO: Change the autogenerated stub
		$this->auth = TestHelpers::create(
			array_get(Definitions::get(), 'auth.site.domain')
		);
		$this->site = $this->auth->site;
		$afi = apnscpFunctionInterceptor::factory($this->auth);
		$this->setApnscpFunctionInterceptor($afi);
		$this->fsRoot = $this->domain_fs_path();
		$this->sourceDir = '/tmp/' . uniqid();
	}

	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown ()
	{
		parent::tearDown();

	}

	public function _testCertChainVerifyApi()
	{
		$prefix = $this->domain_fs_path();
		$cert = file_get_contents($prefix . '/etc/httpd/conf/ssl.crt/server.crt');
		$chain = file_get_contents($prefix . '/etc/httpd/conf/ssl.crt/bundle.crt');
		// test proper orientation cert/chain
		$this->assertEquals(1,$this->call('ssl_verify_certificate_chain', $cert, $chain));
		// test chain/cert order flipped
		$this->assertEquals(-1,$this->call('ssl_verify_certificate_chain', $chain, $cert));
	}

    public function testDownloadChain()
    {
        $cert = self::CRT_CHAIN;
        $file = implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), self::CRT_CHAIN));
        $crt = file_get_contents($file);
        $info = $this->ssl_parse_certificate($crt);
        $this->assertTrue(is_array($info));
        $this->assertArrayHasKey("extensions",$info);
        $this->assertFalse($this->ssl_is_self_signed($crt));
        $chain = $this->ssl_resolve_chain($crt);
        $this->assertEquals(1,$this->ssl_verify_certificate_chain($crt, $chain));
        $this->assertEquals(-1, $this->ssl_verify_certificate_chain($chain, $crt));
    }
}

