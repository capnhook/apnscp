<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class AuthenticationTest extends TestFramework
	{
		protected $filename;

		/**
		 * Seen in metricscron
		 */
		public function testImpliedAuth()
		{
			// authenticated as admin
			session_write_close();
			$auth = \Auth::autoload();
			$auth->resetID(\apnscpSession::init()->create_sid());
			$this->assertNull(\Auth::profile()->username, 'Username in nulled state');
			$this->assertNotSame(\apnscpFunctionInterceptor::init(), \apnscpFunctionInterceptor::init(true));
			$this->assertTrue($auth->authenticate());
			$this->assertSame(
				Auth::get_admin_login(),
				\apnscpFunctionInterceptor::init()->common_whoami(),
				'afi created with proper authentication data'
			);
		}

		public function testExplicitAuth()
		{
			// authenticated as admin
			session_write_close();
			\Auth::use_handler_by_name('CLI');
			$auth = \Auth::import('CLI');
			$auth->authenticate();
			$auth->resetID(\apnscpSession::init()->create_sid());
			$this->assertNull(\Auth::profile()->username, 'Username in nulled state');
			$this->assertNotSame(\apnscpFunctionInterceptor::init(), \apnscpFunctionInterceptor::init(true));
			$this->assertTrue($auth->authenticate());
			$this->assertSame(
				Auth::get_admin_login(),
				\apnscpFunctionInterceptor::init()->common_whoami(),
				'afi created with proper authentication data'
			);
		}
	}

