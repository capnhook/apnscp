<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */

	use Opcenter\Process;

	require_once dirname(__FILE__) . '/../TestFramework.php';

	class FileDescriptorLeakageTest extends TestFramework
	{
		public function testLeakage() {
			$proc =  Util_Process_Sudo::instantiateContexted(
				\Auth::context(\Auth::get_admin_login())
			);
			$proc->setUser('bin');
			$ret = $proc->run('ls -l1 /proc/self/fd');
			$this->assertTrue($ret['success']);
			// hold open socket
			$db = MySQL::initialize();
			$db->ping();
			array_map(function ($line) {
				$this->assertNotContains('socket', $line);
			}, explode("\n", $ret['output']));
			$this->assertGreaterThan(0, count(array_filter(Process::descriptors(posix_getpid()), function ($link) {
				return 0 === strpos($link, "socket:");
			})));
			$this->assertSame('1', $db->query("SELECT 1")->fetch_row()[0], 'Database connection retained');
		}

		public function testTeeUsage() {
			$auth = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));

			$proc = Util_Process_Sudo::instantiateContexted($auth);
			$proc->setOption('tee', $file = tempnam(TEMP_DIR, 'unit-test'));
			$ret = $proc->run('whoami');
			$this->assertTrue($ret['success']);
			$this->assertSame($auth->username, trim((string)file_get_contents($file)));
		}
	}