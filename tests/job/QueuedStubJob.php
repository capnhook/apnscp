<?php
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2018
 */

declare(strict_types=1);

namespace tests\job;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Event;
use Lararia\Jobs\Job;
use Laravel\Horizon\Events\JobFailed;

class QueuedStubJob extends Job implements ShouldQueue {
	const DEFAULT_QUEUE_NAME = 'high';
    protected $msg;
    protected $filename;

    public function __construct(string $msg)
    {
        $this->msg = $msg;
        $this->filename = tempnam(TEMP_DIR, 'queued-stub-test');
        unlink($this->filename);
    }

    public function fire()
    {
        $filename = $this->getFilename();
        Event::listen(JobFailed::class, function ($job) use($filename) {
            file_put_contents($filename, $this->getLog()[0]['message']);
            return null;
        });
        return error($this->msg);
    }

    public function getFilename() {
        return $this->filename;
    }
}