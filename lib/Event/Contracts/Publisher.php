<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	/**
	 * Interface Messenger
	 *
	 * An interface for classes that use Catholic events
	 * Communicates back the caller to the callee
	 *
	 * @package Event\Contracts
	 */

	namespace Event\Contracts;

	interface Publisher
	{
		public function getEventArgs();

	}

