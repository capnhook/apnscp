<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Mail;

	use Opcenter\Contracts\Virtualizable;
	use Opcenter\Filesystem;
	use Opcenter\Role\User;

	class Storage implements Virtualizable
	{
		const MAILDIR_HOME = 'Mail';
		const BASE_FOLDERS = ['Spam', 'Trash', 'Sent', 'Drafts'];

		protected $root;

		public function __construct(string $root = null)
		{
			$this->root = $root;
		}

		public static function bindTo(string $root = '/')
		{
			$instance = new static($root);

			return $instance;
		}

		public static function mailbox2Maildir(string $mailbox): string
		{
			$mailbox = str_replace(['/', '\\'], '.', trim($mailbox));
			if ($mailbox[0] !== '.') {
				$mailbox = '.' . $mailbox;
			}

			return $mailbox;
		}

		public function install(string $user, \apnscpFunctionInterceptor $afi): bool
		{
			$pwd = User::bindTo($this->root);
			$path = $this->root . DIRECTORY_SEPARATOR . $afi;
			Filesystem::mkdir($user);
		}

		/**
		 * Create Maildir-compatible storage for named folder
		 *
		 * @param string     $path  Maildir storage path
		 * @param string|int $user  user or uid
		 * @param string|int $group group or gid
		 * @return bool
		 */
		public function createMaildir(string $path, $user, $group): bool
		{
			if (!\is_int($user)) {
				if (!($pwd = User::bindTo($this->root)->getpwnam($user))) {
					return error("cannot create maildir, specified user `%s' does not exist", $user);
				}
				$user = $pwd['uid'];
			}

			if (!\is_int($group)) {
				if (false === ($grpwd = posix_getgrnam($group))) {
					return error("cannot create maildir, specified group `%s' does not exist", $group);
				}
				$group = $grpwd['gid'];
			}

			$folders = ['', 'new', 'cur', 'tmp'];

			if (!file_exists($tmp = \dirname($this->root . $path))) {
				return error("Mail home `%s' does not exist", $tmp);
			}
			$created = false;
			foreach ($folders as $f) {
				$f = $this->root . $path . DIRECTORY_SEPARATOR . $f;
				if (file_exists($f)) {
					continue;
				}
				if (!\Opcenter\Filesystem::mkdir($f, $user, $group, 0700)) {
					return error("failed to create directory `%s'", $f);
				}
				$created = true;
			}
			$sname = basename($path);
			// ignore root maildir, all descendents will be prefixed with "."
			if ($sname[0] !== '.') {
				return true;
			}
			$subscriptions = \dirname($this->root . $path) . DIRECTORY_SEPARATOR . 'subscriptions';
			$sname = ltrim($sname, '.');
			if (!file_exists($subscriptions)) {
				Filesystem::touch($subscriptions, $user, $group, 0600);
				$contents = array();
			} else {
				$contents = file($subscriptions, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			}
			if ($created && false !== \in_array($sname, $contents)) {
				return info("mailbox `%s' already subscribed", $sname);
			}
			$contents[] = $sname;
			file_put_contents($subscriptions, join("\n", $contents) . "\n");

			return \Opcenter\Filesystem::chogp($subscriptions, $user, $group, 0600);
		}
	}