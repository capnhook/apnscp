<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Mail\Services;

	use Opcenter\Contracts\Virtualizable;
	use Opcenter\Mail\Contracts\ListProvider;

	class Majordomo implements ListProvider, Virtualizable
	{
		const MAILING_LIST_HOME = '/var/lib/majordomo';

		protected $root;

		public function __construct(string $root = '/')
		{
			$this->root = $root;
		}

		public static function bindTo(string $root = '/')
		{
			return new static($root);
		}


		public function getListHome(): string
		{
			return $this->root . '/' . self::MAILING_LIST_HOME;
		}
	}
