<?php declare(strict_types=1);

namespace Opcenter\Mail\Services\Spamassassin;

class Filter {
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;

	const TEMPLATE = '/usr/share/spamassassin/user_prefs.template';
	/**
	 * @var void
	 */
	protected $home;

	protected function __construct()
	{
		$this->home = $this->user_get_home();
	}

	public function exists(): bool
	{
		return $this->file_exists($this->home . '/.spamassassin/user_prefs');
	}

	public function create(): bool
	{
		if (!$this->file_exists($this->home . '/.spamassassin')) {
			$this->file_create_directory($this->home . '/.spamassassin');
		}

		$stat = $this->file_stat($prefs = $this->home . '/.spamassassin/user_prefs');
		if ($stat && $stat['link']) {
			return error("user_prefs is an irregular file");
		}
		return $this->file_put_file_contents($prefs, $this->template());

	}

	public function template(): string
	{
		if (!file_exists(self::TEMPLATE)) {
			return '';
		}

		return file_get_contents(self::TEMPLATE);
	}
}