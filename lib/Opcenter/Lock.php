<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter;

	class Lock
	{

		const GLOBAL_LOCK = 'opcenter.lock';
		const LOCK_TIMEOUT = 120;

		/** @var resource global lock fp */
		static private $fp;
		/** @var bool minor optimization to prevent registering multiple cleanup tasks */
		static private $registered = false;

		/**
		 * Acquire global opcenter lock
		 *
		 * @param bool $block block for lock
		 * @return bool
		 */
		public static function lock($block = true): bool
		{
			$path = run_path(self::GLOBAL_LOCK);
			$fp = fopen($path, 'c');
			if (!$fp) {
				fatal("failed to acquire global lock `%s'", $path);
			}
			self::$fp = $fp;
			for ($i = 0; $i < self::LOCK_TIMEOUT; $i++) {
				$status = flock(self::$fp, LOCK_EX | LOCK_NB);
				if (!$block || $status) {
					break;
				}
				sleep(1);
			}
			if ($block && !$status) {
				fatal("failed to acquire global lock `%s' - timedout", $path);
			} else if (!self::$registered) {
				register_shutdown_function(
					static function ($fp, $file) {
						if (\is_resource($fp)) {
							fclose($fp);
						}
						if (file_exists($file)) {
							unlink($file);
						}
					}, self::$fp, run_path(self::GLOBAL_LOCK));
				self::$registered = true;
			}

			if ($status) {
				ftruncate($fp, 0);
				rewind($fp);
				fwrite($fp, (string)getmypid());
			}

			return $status;
		}

		/**
		 * Release global opcenter lock
		 *
		 * @return bool
		 */
		public static function unlock(): bool
		{
			if (null === self::$fp) {
				return false;
			}
			fclose(self::$fp);
			self::$fp = null;

			return true;
		}

		/**
		 * Verify if lock is active
		 *
		 * @return bool
		 */
		public static function locked(): bool
		{
			if (null === self::$fp) {
				return false;
			}
			$status = flock(self::$fp, LOCK_EX | LOCK_NB);
			if (!$status) {
				return false;
			}
			flock(self::$fp, LOCK_UN);

			return true;
		}
	}