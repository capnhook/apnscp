<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Argos\Backends;

	use Opcenter\Argos\Backend;

	class Pushjet extends Backend
	{
		protected $secret;
		protected $endpoint;
		protected $level;
		protected $link;

		public function getAuthentication()
		{
			return $this['secret'];
		}

		public function setAuthentication(...$vars): bool
		{
			$this['secret'] = $vars[0];

			return true;
		}
	}