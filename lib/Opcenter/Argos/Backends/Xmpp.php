<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Argos\Backends;

	use Opcenter\Argos\Backend;

	class Xmpp extends Backend
	{
		protected $jid;
		protected $password;
		protected $recipient;

		// optional
		protected $hostname;
		protected $port;
		protected $path_to_certs;
		protected $mtype;

		public function getAuthentication()
		{
			return [
				'jid'       => $this['jid'],
				'password'  => $this['password'],
				'recipient' => $this['recipient']
			];
		}

		public function setAuthentication(...$vars): bool
		{
			if (isset($vars[2])) {
				// config_set argos.auth xmpp JID PASSWORD RECIPIENT RECIPIENT
				$vars = [
					'jid'       => $vars[0],
					'password'  => $vars[1],
					'recipient' => $vars[2]
				];
			} else {
				// config_set argos.auth xmpp '[jid:JID,password:PASSWORD,recipient:RECIPIENT]'
				$vars = $vars[0];
			}
			foreach (['jid', 'password', 'recipient'] as $token) {
				if (!isset($vars[$token])) {
					return error("Missing authentication item `%s'", $token);
				}
			}

			$this['jid'] = $vars['jid'];
			$this['password'] = $vars['recipient'];
			$this['recipient'] = $vars['recipient'];

			return true;
		}
	}