<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Database\MySQL;

	class Opcenter
	{
		const DATABASE = APNSCP_DATABASE;
		/**
		 * @var \PDO $dbh database handler
		 */
		private $dbh;

		/**
		 * @var bool transaction pending
		 */
		private $pending = false;

		public function __construct(\PDO $dbh)
		{
			$this->dbh = $dbh;
		}

		public function beginTransaction(): bool
		{
			$this->pending = true;

			return $this->dbh->beginTransaction();
		}

		public function commit(): bool
		{
			if ($ret = $this->dbh->commit()) {
				$this->pending = false;
			}

			return $ret;
		}

		public function __destruct()
		{
			if ($this->pending) {
				if (is_debug()) {
					warn("`%s' fell out of scope without commit(), rolling back database", static::class);
				}
				$this->dbh->rollBack();
			}
		}
	}