<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
	 */

	namespace Opcenter\Admin\Settings\Net;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Net\Ip4;

	class Nameservers implements SettingsInterface
	{

		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			$val = (array)$val;

			foreach ($val as $ns) {
				$res = \Net_Gethost::gethostbyname_t('google.com', 2000, [$ns]);
				if (!$res) {
					return error("Nameserver `%s' did not " .
						'return authoritative answer for google.com', $ns);
				}
			}

			$cfg = new Config();
			$cfg['use_robust_dns'] = true;
			$cfg['dns_robust_nameservers'] = $val;
			if (isset($cfg['apnscp_nameservers'])) {
				warn(
					'apnscp_nameservers is set in %s. Remove this setting to update panel nameservers as well.',
					Bootstrapper::RUNTIME_VARS
				);
			}
			$cfg->sync();

			return Bootstrapper::run('common/update-config', 'apnscp/filesystem-checks', 'apnscp/bootstrap');
		}

		public function get()
		{
			$lines = file('/etc/resolv.conf', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			return array_values(array_filter(array_map(static function($line) {
				$line = trim($line);
				if (false === strpos($line, ' ')) {
					return null;
				}
				[$dir, $val] = preg_split('/\s+/', trim($line), 2);
				if ($dir !== 'nameserver') {
					return null;
				}
				return $val;
			}, $lines)));
		}

		public function getHelp(): string
		{
			return 'Get nameservers for server';
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			if (!Ip4::enabled()) {
				return ['2606:4700:4700::1111', '2606:4700:4700::1001'];
			}

			return ['1.0.0.1', '1.1.1.1'];
		}

	}
