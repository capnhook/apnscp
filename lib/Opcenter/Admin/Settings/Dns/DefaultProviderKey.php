<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace Opcenter\Admin\Settings\Dns;

	use Opcenter\Admin\Settings\Cp\Bootstrapper;
	use Opcenter\Admin\Settings\Cp\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\CliParser;

	class DefaultProviderKey implements SettingsInterface
	{
		const TYPE = 'dns';

		public function get(...$var)
		{
			return (new Config())->get(static::TYPE, 'provider_key');
		}

		/**
		 * Set provider
		 *
		 * @param       $val
		 * @return bool
		 */
		public function set($val): bool
		{
			if ($val === null) {
				$val = 'null';
			}
			$val = CliParser::collapse($val);
			return (new Config())->set(static::TYPE, 'provider_key',
					$val) && (new Bootstrapper())->set(static::TYPE . '_default_provider_key', $val);
		}

		public function getHelp(): string
		{
			return 'Default DNS provider authentication token';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return '';
		}
	}