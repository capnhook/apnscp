<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Admin\Settings\Argos;

	use Opcenter\Admin\Settings\SettingsInterface;

	class Test implements SettingsInterface
	{
		public function get()
		{
			return 'Sample input: <test message> <backend>';
		}

		/**
		 * Set Argos authentication
		 *
		 * Valid options: name, token, user
		 *
		 * @param string $val
		 * @param string $backend backend to relay
		 * @return bool
		 */
		public function set($val = 'Test message from Argos', $backend = 'default'): bool
		{
			$afi = \apnscpFunctionInterceptor::factory(\Auth::context(null));
			return $afi->argos_send($val, $backend);
		}

		public function getHelp(): string
		{
			return 'Relay a test message through optionally named backend';
		}

		public function getValues()
		{
			return 'string ?string';
		}

		public function getDefault()
		{
			return ['Test message from Argos', 'default'];
		}
	}