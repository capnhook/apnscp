<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Http\Apache;
	use Opcenter\Map;

	class Cache implements SettingsInterface
	{

		public function set($val): bool
		{
			if ($val && !\in_array($val, $this->getValues(), true)) {
				return error("unknown setting `%s', valid settings: %s",
					$val,
					implode(',', $this->getValues())
				);
			}
			if (!$val) {
				$val = '';
			}
			if ($val === $this->get()) {
				return true;
			}

			if (!$val) {
				$spec = '-DNO_CACHE';
			} else if ($val === 'disk') {
				$spec = '-DCACHE_DISK';
			} else if ($val === 'memory') {
				$spec = '-DCACHE_MEMORY';
			} else {
				$spec = '-DCACHE_BOTH';
			}
			$ini = Map::write(Apache::SYSCONFIG_FILE);
			$opts = (string)$ini->section(null)->quoted(true)->offsetGet('OPTIONS');
			// @XXX implicit /g broken, explicitly provide
			$opts = preg_replace('/(?:^|\s+)-D(?:NO_CACHE|CACHE_DISK|CACHE_MEMORY|CACHE_BOTH)\s*/', '',
					$opts) . ' ' . $spec;

			return $ini->set('OPTIONS', ltrim($opts)) && Apache::restart();
		}

		public function getValues()
		{
			return ['memory', 'disk', 'memory+disk', false];
		}

		public function get()
		{
			$ini = Map::load(Apache::SYSCONFIG_FILE);
			if (!($opts = $ini->section(null)->offsetGet('OPTIONS'))) {
				return true;
			}

			if (false !== strpos($opts, '-DNO_CACHE')) {
				return '';
			}
			if (false !== strpos($opts, '-DCACHE_MEMORY')) {
				return 'memory';
			}
			if (false !== strpos($opts, '-DCACHE_BOTH')) {
				return 'memory+disk';
			}

			return 'disk';
		}

		public function getHelp(): string
		{
			return 'Change upstream HTTP caching method';
		}

		public function getDefault()
		{
			return 'disk';
		}
	}