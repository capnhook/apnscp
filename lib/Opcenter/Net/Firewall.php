<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, April 2018
	 */


	namespace Opcenter\Net;

	use Opcenter\Net\Firewall\Contracts\FirewallInterface;
	use Opcenter\Net\Firewall\Rule;

	/**
	 * Class Firewall
	 *
	 * A simple firewall to block incoming connections
	 *
	 * @package Opcenter\Net
	 */
	class Firewall
	{
		use \NamespaceUtilitiesTrait;

		/**
		 * @var FirewallInterface
		 */
		protected $driver;

		public function __construct($driver = \Rampart_Module::FAIL2BAN_DRIVER)
		{
			$this->driver = self::appendNamespace('Firewall\\' . ucwords($driver) . '\\FirewallConnector');
		}

		public function getEntriesFromGroup(string $group = null): Rule
		{
			return $this->driver::getEntriesFromGroup($group);
		}


		/**
		 * Get all firewall chains
		 *
		 * @return array
		 */
		public function groups(): array
		{
			return $this->driver::groups();
		}

		/**
		 * Get IP entries from chains
		 *
		 * @param null|string $chain optional chain to filter
		 * @return array
		 */
		public function getEntriesFromChain(string $chain = null): array
		{
			$records = $this->driver::getEntriesFromGroup($chain);
			if ($chain) {
				$records = [
					$chain => $records
				];
			}

			return $chain ? $records[$chain] ?? [] : $records;
		}

		/**
		 * Append a rule to the end of a chain
		 *
		 * @param string $group
		 * @param string $host
		 * @return bool
		 */
		public function append(string $group, string $host): bool
		{
			$rule = $this->makeRule(['group' => $group, 'host' => $host]);

			return $this->driver::append($rule);
		}

		private function makeRule(array $ctor = []): Rule
		{
			return new Rule($ctor);
		}

		/**
		 * Insert a rule to the beginning of a chain
		 *
		 * @param string $group
		 * @param string $host
		 * @return bool
		 */
		public function insert(string $group, string $host): bool
		{
			$rule = $this->makeRule(['group' => $group, 'host' => $host]);

			return $this->driver::insert($rule);
		}

		/**
		 * Remove rule by index
		 *
		 * @param string $group
		 * @param string $host
		 * @return bool
		 */
		public function remove(string $group, string $host): bool
		{
			$rule = $this->makeRule(['group' => $group, 'host' => $host]);

			return $this->driver::remove($rule);
		}
	}