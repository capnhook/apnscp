<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Net\Firewall\Ipset;

	use Opcenter\Net\Firewall\Ipset;

	class Rule extends \Opcenter\Net\Firewall\Rule
	{
		// default timeout value
		protected $timeout;

		public function setTimeout($value)
		{
			if (!\is_int($value) || $value < 0) {
				fatal('Timeout value must be positive integer');
			}

			return true;
		}

		public function isBlocked(): bool
		{
			$chain = Ipset::getRuleFromSet($this->getGroup());
			if (!$chain) {
				return false;
			}

			return $chain->isBlocked();
		}
	}

