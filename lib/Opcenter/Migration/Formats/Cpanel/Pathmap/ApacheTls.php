<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


use Event\Cardinal;
use Event\Events;
use Opcenter\Crypto\Ssl;
use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
use Opcenter\SiteConfiguration;

class ApacheTls extends ConfigurationBuilder {
	public function parse(): bool
	{
		if (false === ($path = $this->getDirectoryEnumerator()->getPathname())) {
			return true;
		}

		Cardinal::register([\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
			static function ($event, SiteConfiguration $p) use ($path) {
				// @TODO 3.0 doesn't have Opcenter\Crypto\Ssl
				// 3.1 does, let's dumb this down until 3.0 is sunset
				$pem = file_get_contents($path);
				$pub = $pkey = null;

				if (!openssl_pkey_export(openssl_pkey_get_private($pem), $pkey)) {
					return error('Failed to export private key from PEM: %s', openssl_error_string());
				}
				if (!openssl_x509_export(openssl_x509_read($pem), $pub)) {
					return error('Failed to export public key from PEM: %s', openssl_error_string());
				}
				$chain = Ssl::splitChain($pem);
				if ($chain) {
					array_shift($chain);
				}
				$p->getSiteFunctionInterceptor()->ssl_install($pkey, $pub, implode("\n", $chain));
			});
		return true;
	}

}

