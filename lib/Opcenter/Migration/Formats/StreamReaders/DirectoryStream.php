<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */


namespace Opcenter\Migration\Formats\StreamReaders;

use Opcenter\Filesystem;
use Opcenter\Migration\Contracts\BackupStreamInterface;
use Opcenter\Migration\Locker;

class DirectoryStream extends \RecursiveDirectoryIterator implements \RecursiveIterator, \SeekableIterator, \Countable, \ArrayAccess, BackupStreamInterface {
	protected $path;
	/**
	 * @var Locker
	 */
	protected $lock;

	public function __construct($path, $flags = 0)
	{
		if (!is_dir($path)) {
			fatal('Path %s must be directory', $path);
		}
		$myflags = \FilesystemIterator::KEY_AS_FILENAME | \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS;
		parent::__construct($path, $flags|$myflags);
		$this->path = $path;
	}

	public static function fromArchive(string $file, Locker $locker = null): self {
		if (!file_exists($file) || !is_file($file)) {
			fatal("File `%s' does not exist", $file);
		}

		$gzipProgram = $gzipArg = null;
		$tmp = substr($file, 0, -3);
		if ($tmp === '.gz') {
			$gzipProgram = ArchiveStream::getBin();
			$gzipArg = '--use-compress-program=';
		}
		info('Archive is compressed. Decompressing %s to intermediary %s. This may take some time...',
			$file, $tmp);
		$extractionPath = tempnam(\dirname($file), 'migex');
		if (!unlink($extractionPath) || !Filesystem::mkdir($extractionPath, 0, 0, 0700)) {
			fatal("Failed to create temporary directory `%s' for extraction", $extractionPath);
		}
		$ret = \Util_Process_Safe::exec('tar -C %s %s%s -xaf %s',  $extractionPath, $gzipArg, $gzipProgram, $file);
		register_shutdown_function(static function() use ($extractionPath, $locker) {
			if (0 !== strncmp(\basename($extractionPath), 'migex', 5)) {
				return error("??? expected `migex' got %s", \basename($extractionPath));
			}

			$ret = \Util_Process_Safe::exec('rm --one-file-system -rf %s', $extractionPath);
			if ($ret['success']) {
				debug("Removed temporary location `%s'", $extractionPath);
			} else {
				warn("Failed to remove temporary location `%s' - files may be left behind", $extractionPath);
			}
			$locker->unlock();
		});
		if (!$ret['success']) {
			fatal('Failed to temporarily decompress %s: %s', $file, $ret['stderr']);
		}
		if (\count($files = glob("$extractionPath/*")) === 1) {
			return new static($files[0]);
		}
		return new static($extractionPath);
	}

	public function getPrefix(): string {
		return $this->path;
	}

	/**
	 * Hold lock until archive can be recompressed
	 *
	 * @param Locker $lock
	 */
	public function bind(Locker $lock): void
	{
		$this->lock = $lock;
	}

	public function archivePath(string $file): \DirectoryIterator {
		return new \DirectoryIterator($this->getPrefix() . DIRECTORY_SEPARATOR . $file);
	}

	public function externalExtract(string $subdir, string $dest, int $uid = 0, int $gid = 0): bool
	{
		$ret = \Util_Process_Safe::exec('rsync -a --chown=%d:%d %s/%s/ %s', $uid, $gid, $this->getPrefix(), $subdir, $dest);
		return $ret['success'] ?: error($ret['stderr']);
	}

	public function offsetExists($offset)
	{
		return file_exists($this->path . DIRECTORY_SEPARATOR . $offset);
	}

	public function offsetGet($offset)
	{
		return new \SplFileInfo($this->path . DIRECTORY_SEPARATOR . $offset);
	}

	public function offsetSet($offset, $value)
	{
		file_put_contents($this->path . DIRECTORY_SEPARATOR . $offset, $value);
	}

	public function offsetUnset($offset)
	{
		file_exists($this->path . DIRECTORY_SEPARATOR . $offset) && unlink($this->path . DIRECTORY_SEPARATOR . $offset);
	}

	public function count()
	{
		return \count(glob($this->path . '/*', GLOB_NOSORT));
	}

	public function seek($position)
	{
		// TODO: Implement seek() method.
	}

}