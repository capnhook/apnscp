<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Crypto;

use Opcenter\License;

/**
 * Class SimpleCrypt
 *
 * Encrypt/decrypt
 *
 * @package Opcenter\Crypto
 */
class NaiveCrypt implements \Serializable {
	const CIPHER = 'aes-256-gcm';
	const TAG_COOKIE_NAME = 'sso-tag';
	/**
	 * @var string crypted data
	 */
	private $crypted;
	/**
	 * @var string IV
	 */
	private $iv;

	public function __construct(string $token)
	{
		$ivlen = openssl_cipher_iv_length(static::CIPHER);
		$this->iv = $this->ivInit($ivlen);
		$this->crypted = openssl_encrypt($token, static::CIPHER, AUTH_SECRET, 0, $this->iv, $tag);
		$encoded = base64_encode($tag);
		if (!headers_sent()) {
			setcookie(static::TAG_COOKIE_NAME, $encoded);
		}
		$_COOKIE[static::TAG_COOKIE_NAME] = $encoded;
	}

	public function serialize()
	{
		return serialize([
			'crypted' => $this->crypted,
			'iv'      => base64_encode($this->iv)
		]);
	}

	public function unserialize($serialized)
	{
		$data = \Util_PHP::unserialize($serialized);
		$this->crypted = $data['crypted'];
		$this->iv = base64_decode($data['iv']);
	}


	public function __debugInfo()
	{
		return [];
	}

	/**
	 * Get IV bytes
	 *
	 * @param int $length
	 * @return string
	 */
	private function ivInit(int $length): string {
		return openssl_random_pseudo_bytes($length);
	}

	/**
	 * Get decrypted string
	 *
	 * @return string|null
	 */
	public function get(): ?string {
		if (!isset($_COOKIE[self::TAG_COOKIE_NAME])) {
			return null;
		}
		$tag = base64_decode($_COOKIE[self::TAG_COOKIE_NAME], true);
		if (false === $tag) {
			return null;
		}
		$password = openssl_decrypt($this->crypted, static::CIPHER, AUTH_SECRET, 0, $this->iv, $tag);
		if (false === $password) {
			// bogus tag
			return null;
		}

		return $password;
	}
}
