<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;


	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
	use Opcenter\Provisioning\Traits\GroupCreationTrait;
	use Opcenter\SiteConfiguration;

	class PostgreSQL
	{
		use FilesystemPopulatorTrait;
		use GroupCreationTrait;

		const TEMPLATE_FILES = [];
		const TEMPLATE_DIRECTORIES = [
			'/var/lib/pgsql' => ['postgres', null, 02750]
		];
		const SUPPLEMENTAL_GROUPS = [
			'postgres' => ['postgres', null]
		];

		public static function configTemplate(string $user, string $password): string
		{
			return '[client]' . "\n" .
				"user=$user" . "\n" .
				"password=$password" . "\n";
		}

		protected static function getPassword(SiteConfiguration $configuration)
		{
			$afi = \apnscpFunctionInterceptor::factory($configuration->getAuthContext());
			if ($p = $afi->pgsql_get_password()) {
				return $p;
			}
			$checkers = [
				['pgsql', 'password'],
				['mysql', 'password'],
				['siteinfo', 'tpasswd']
			];
			foreach ($checkers as $c) {
				if ($p = $configuration->getServiceValue(...$c)) {
					return $p;
				}
			}

			return Password::generate();

		}
	}