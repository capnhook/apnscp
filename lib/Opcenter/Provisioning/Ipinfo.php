<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\SiteConfiguration;

	class Ipinfo
	{
		const SERVICE = 'ipinfo';

		public static function getAddresses(SiteConfiguration $container): array
		{
			$field = 'ipaddrs';
			$proxyname = static::SERVICE === self::SERVICE ? 'proxyaddrs' : 'proxy6addrs';
			if ($container->getServiceValue(static::SERVICE, 'namebased')) {
				$field = 'nbaddrs';
			}

			return (array)($container->getServiceValue('dns', $proxyname) ?: $container->getServiceValue(static::SERVICE, $field));
		}

	}