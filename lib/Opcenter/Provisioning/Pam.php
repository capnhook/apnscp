<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Provisioning;

	use Opcenter\Process;
	use Opcenter\SiteConfiguration;

	class Pam
	{
		protected $svc;
		protected $pamh;

		public function __construct(SiteConfiguration $svc)
		{
			$this->svc = $svc;
			$this->pamh = new \Util_Pam($this->svc->getAuthContext());
		}

		/**
		 * Create PAM configuration
		 *
		 * @param string $pamsvc
		 * @return self
		 */
		public function enable(string $pamsvc): self
		{

			$file = $this->pamh->getControlFile($pamsvc);
			if (!file_exists($file)) {
				touch($file);
			}
			$admin = $this->svc->getServiceValue('siteinfo', 'admin_user');
			$this->pamh->add($admin, $pamsvc);

			return $this;
		}

		/**
		 * Disable PAM service for site
		 *
		 * @param string $pamsvc
		 * @return self
		 */
		public function disable(string $pamsvc): self
		{
			$file = $this->pamh->getControlFile($pamsvc);
			if (!file_exists($file) || !unlink($file)) {
				warn("pam service `%s' not enabled", $pamsvc);
			}

			return $this;
		}

		/**
		 * Service exists
		 *
		 * @param string $service PAM service
		 * @return bool
		 */
		public function exists(string $service): bool
		{
			return file_exists($this->pamh->getControlFile($service));
		}

		/**
		 * Terminate processes under a PAM service
		 *
		 * @param string $pmask
		 * @return self
		 */
		public function terminate(?string $pmask): self
		{
			$group = $this->svc->getServiceValue('siteinfo', 'admin');
			$processes = Process::matchGroup($group);
			foreach ($processes as $process) {
				if ($pmask && !Process::pidMatches($process, $pmask)) {
					continue;
				}
				Process::kill($process, SIGKILL);
			}

			return $this;
		}
	}