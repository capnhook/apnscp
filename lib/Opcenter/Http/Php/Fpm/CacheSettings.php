<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Opcenter\Http\Php\Fpm;

class CacheSettings implements \IteratorAggregate, \ArrayAccess {
	/**
	 * @var array
	 */
	protected $settings;

	public function __construct(array $settings = []) {
		$this->settings = $settings;
	}

	/**
	 * Get size in MB
	 *git di
	 * @return int
	 */
	public function getSize(): int
	{
		return $this->settings['directives']['opcache.memory_consumption'] / 1024 / 1024;
	}

	public function getIterator()
	{
		return $this->settings;
	}

	public function offsetExists($offset)
	{
		return isset($this->settings[$offset]);
	}

	public function offsetGet($offset)
	{
		return $this->settings[$offset];
	}

	public function offsetSet($offset, $value)
	{
		$this->settings[$offset] = $value;
	}

	public function offsetUnset($offset)
	{
		unset($this->settings);
	}


}