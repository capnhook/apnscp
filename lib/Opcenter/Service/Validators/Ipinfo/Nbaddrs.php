<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Ipinfo;

	use Opcenter\Account\Enumerate;
	use Opcenter\Net\Iface;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Validators\Common\IpValidation;
	use Opcenter\SiteConfiguration;

	class Nbaddrs extends IpValidation implements AlwaysValidate
	{
		const VALUE_RANGE = '[null, [addr1, addr2...]]';
		const DESCRIPTION = 'Assign shared addresses to account. Leave blank to select round-robin';

		public function valid(&$value): bool
		{
			if (!$this->ctx['enabled'] || !$this->ctx['namebased']) {
				$value = [];

				return true;
			} else if ($value && $value === $this->ctx->getOldServiceValue(null,
					'nbaddrs') && null === $this->ctx->getNewServiceValue(null, 'namebased')) {
				return true;
			}
			if ($value && !\is_array($value)) {
				$value = [$value];
			}
			if (empty($value) && $this->ctx['namebased']) {
				$pool = $this->getPool();
				if (!$pool) {
					return error("no IP addresses found to allocate to site `%s'",
						$this->ctx->getServiceValue('siteinfo', 'domain'));
				}
				$sitecount = \count(Enumerate::sites());
				$ip = $pool[$sitecount % \count($pool)];
				$value = [$ip];
			}
			foreach ($value as $ip) {
				if (!\in_array($ip, $this->getPool(), true)) {
					return error("Requested IP `%s' is not part of namebased pool", $ip);
				}
			}

			if (!$this->ctx->hasOld()) {
				foreach ($value as $ip) {
					if (!Iface::bound($ip)) {
						// devices behind NAT can publicize addresses, warn instead of vomit
						// @todo determine warn/error on private network address?
						return warn("IP address `%s' not found on any interface on server - server on private network?", $ip);
					}
				}
			}

			return true;
		}

		protected function getPool(): array
		{
			$class = '\\Opcenter\\Net\\' . (static::class === self::class ? 'Ip4' : 'Ip6');
			return $class::nb_pool();
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return true;
		}


	}