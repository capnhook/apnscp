<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Mysql;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Account\Edit;
	use Opcenter\Database\DatabaseCommon;
	use Opcenter\Database\Mapper;
	use Opcenter\Database\MySQL;
	use Opcenter\Service\Contracts\AlwaysRun;
	use Opcenter\Service\Contracts\ServiceExplicitReconfiguration;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;

	class Dbaseprefix extends ServiceValidator implements ServiceReconfiguration, ServiceInstall, ServiceExplicitReconfiguration, AlwaysRun
	{
		const GROUPED_SERVICES = [
			'mysql',
			'pgsql'
		];

		public function valid(&$value): bool
		{
			if (!$value && !$this->ctx->getServiceValue(null, 'enabled')) {
				return true;
			}

			if ($value && $this->ctx->isDelete()) {
				// force discard of databases
				$this->ctx['enabled'] = 1;
			}

			if (!$value) {
				foreach (array_diff(static::GROUPED_SERVICES, [$this->ctx->getModuleName()]) as $svc) {
					if (null !== ($prefix = $this->ctx->getServiceValue($svc, 'dbaseprefix'))) {
						// default to other module prefix if none provided
						$value = $prefix;
						break;
					}
				}

				if (!$value) {
					$value = \Opcenter\Database\MySQL::suggestPrefix($this->ctx->getServiceValue('siteinfo', 'domain'), $this->site);
				}
				info("no %(type)s database prefix specified - suggesting `%(prefix)s'",
					['type' => strtolower($this->ctx->getModuleName()), 'prefix' => $value]
				);
			}

			if ($value[-1] !== '_') {
				$value .= '_';
			}

			$class = '\Opcenter\Database\\' . DatabaseCommon::canonicalizeBrand(strtolower($this->ctx->getModuleName()));
			$check = $this->ctx->isEdit() ? $this->site : null;
			if ($class::prefixExists($value, $check)) {
				return error("database prefix `%s' already in use", $value);
			}
			$re = \Regex::compile(\Regex::SQL_PREFIX, ['max' => MySQL::fieldLength('db')]);
			if (!preg_match($re, $value)) {
				return error("database prefix `%s' is invalid", $value);
			}
			// 3 is arbitrary, allows 26^2 db names
			$maxlen = MySQL::fieldLength('user') - 3;
			if (($len = \strlen($value)) > $maxlen) {
				return error("database prefix length must not exceed `%s', %s has length %s",
					$maxlen, $value, $len);
			}

			return true;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure(null, $this->ctx->getServiceValue(null, $this->getServiceName()), $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($new && !$this->ctx['enabled']) {
				return true;
			}

			$mapper = new Mapper($this->ctx->getModuleName(), 'prefix');
			if ($old) {
				$mapper->unmap($old, $svc->getSite());
			}
			if ($new) {
				$mapper->map($new, $svc->getSite());
			}
			$mapper = null;

			if ($new === $old || !$this->ctx->isEdit()) {
				return true;
			} else if (null === $new && !$this->ctx['enabled'] && $this->ctx->isEdit()) {
				// double throw safety switch engaged
				$this->depopulateGuarded($svc);

			}

			Cardinal::register([Edit::HOOK_ID, Events::END], function ($event, SiteConfiguration $s) {
				$this->freshenSite($s);
			});

			if (!$old || !$new) {
				return true;
			}

			$type = strtolower($this->ctx->getModuleName());
			$class = '\Opcenter\Database\\' . DatabaseCommon::canonicalizeBrand($type);
			$changed = 0;

			$users = $svc->getSiteFunctionInterceptor()->{"${type}_list_users"}();
			foreach (array_keys($users) as $user) {
				$newuser = DatabaseCommon::convertPrefix($user, $old, $new);
				if ($newuser === $user) {
					continue;
				}
				if ($class::renameUser($user, $newuser)) {
					$changed++;
				}
			}
			$class::flushTables();

			// rename backups
			// @TODO move to sql.php?
			$pwd = $svc->getSiteFunctionInterceptor()->user_getpwnam();
			$backupPath = $pwd['home'] . "/${type}_backups/";

			// all files in backup will be owned by user, shortcircuit sec checks and seteuid
			$olduid = posix_getuid();
			posix_seteuid($pwd['uid']);
			defer($_, static function () use ($olduid) {
				posix_seteuid($olduid);
			});
			$basePath = $svc->getAuthContext()->domain_fs_path($backupPath);
			foreach (glob("${basePath}/${old}*", GLOB_NOSORT) as $file) {
				if (!is_file($file)) {
					continue;
				}
				$newFile = $new . substr(basename($file), \strlen($old));
				rename($file, "${basePath}/${newFile}");
			}

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			if ($this->ctx['dbaseprefix'] && !$this->ctx['enabled'] && $this->ctx->isDelete()) {
				// DTSS wrapper
				foreach (['enabled', 'dbaseadmin'] as $svcVar) {
					$instance = $this->ctx->getValidatorClass($svcVar);
					(new $instance($this->ctx, $this->site))->depopulate($svc);
				}
			}
			if ($svc->canInstantiateInterceptor() && !$this->depopulateGuarded($svc)) {
				return false;
			}

			return $this->reconfigure($this->ctx->getServiceValue(null, $this->getServiceName()), null, $svc);
		}

		/**
		 * Conditionally delete users if afi can instantiate
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		private function depopulateGuarded(SiteConfiguration $svc): bool
		{
			if ($this->ctx->isEdit()) {
				$instance = $this->ctx->getValidatorClass('enabled');
				return (new $instance($this->ctx, $this->site))->depopulate($svc);
			}

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}


		public function getDescription(): ?string
		{
			return 'Set ' . DatabaseCommon::canonicalizeBrand($this->ctx->getModuleName()) . " database prefix. Must end with '_'";
		}

	}
