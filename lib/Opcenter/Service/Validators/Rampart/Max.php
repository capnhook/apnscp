<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */


	namespace Opcenter\Service\Validators\Rampart;

	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\ServiceValidator;

	class Max extends ServiceValidator implements DefaultNullable
	{
		public const VALUE_RANGE = '[-1, 0 => 4096]';
		public const DESCRIPTION = 'Maximum number of IP address whitelists.';

		public function getDefault()
		{
			return RAMPART_DELEGATED_WHITELIST;
		}

		/**
		 * Value is valid
		 *
		 * @param $value
		 * @return bool
		 */
		public function valid(&$value): bool
		{
			if ($value === null) {
				$value = -1;
			} else if ($value === DefaultNullable::NULLABLE_MARKER) {
				$value = $this->getDefault();
			}
			if ($value === -1 || $value === 0) {
				return true;
			}
			if (!\is_int($value) || $value > 4096) {
				return false;
			}
			$cnt = $this->ctx->getServiceValue(null, 'whitelist', []);
			if (\count($cnt) > $value) {
				return error('%s,whitelist count is %d. Remove entries before reducing limit', $this->getServiceName(), $cnt);
			}

			return true;
		}
	}
