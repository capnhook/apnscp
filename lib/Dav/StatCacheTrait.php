<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Dav;

	trait StatCacheTrait
	{
		protected $lastCache;

		/**
		 * Pull file from stat cache
		 *
		 * @param string $path base directory
		 * @param string $name filename
		 * @return array|false stat meta or false on failure
		 */
		protected function cacheCheck(string $path, string $name)
		{
			if (null === $this->lastCache) {
				return false;
			}
			if ($this->lastCache['dir'] !== $path) {
				return false;
			}
			if (!isset($this->lastCache['cache'][$name])) {
				return false;
			}

			return $this->lastCache['cache'][$name];
		}

		/**
		 * Purge cache
		 *
		 * @return void
		 */
		protected function invalidateCache(): void
		{
			$this->lastCache = null;
		}

	}