<?php

	namespace Lararia\Console\Commands;

	use Illuminate\Database\Console\Migrations\RollbackCommand;
	use Symfony\Component\Console\Input\InputOption;

	class PlatformRollback extends RollbackCommand
	{
		/**
		 * The console command description.
		 *
		 * @var string
		 */


		public function __construct()
		{
			$migrator = app('migrator');
			parent::__construct($migrator);
		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			$args = [
				'--rollback' => true,
				'--force'    => $this->option('force'),
				'--step'     => $this->option('step')
			];
			if ($this->hasOption('dbonly')) {
				return parent::handle();
			}
			if ($this->hasOption('platformonly')) {
				return Artisan::call('migrate:platform', $args);
			}
			parent::handle();
			Artisan::call('migrate:platform', $args);
		}

		public function getHelp()
		{
			return parent::getHelp() .
				'Default runs db, then platform migration.';
		}

		protected function getOptions()
		{
			return array_merge(parent::getOptions(), [
				['dbonly', null, InputOption::VALUE_NONE, 'Rollback a database migration only.'],
				['platformonly', null, InputOption::VALUE_NONE, 'Rollback a platform migration instead.'],
			]);
		}


	}
