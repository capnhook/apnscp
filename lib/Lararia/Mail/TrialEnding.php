<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Mail;

	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;

	class TrialEnding extends Mailable
	{
		protected $args;

		/**
		 * Create a new message instance.
		 *
		 * @param array $args
		 */
		public function __construct(array $args)
		{
			$this->args = $args;
		}

		public function build()
		{
			return $this->markdown('email.admin.trial-ending',
				$this->args
			)->subject(_(PANEL_BRAND . ' Trial Ending - ' . SERVER_NAME));
		}
	}
