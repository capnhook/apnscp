<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2021
 */

namespace Auth\UI;

use Opcenter\Auth\Password;
use Opcenter\Http\Apnscp;
use Opcenter\Process;

class SecureAccessKey {

	private const SECURE_ACCESS_KEY = 'cp.secKey';
	public const SECKEY_ENV = 'SECKEY';

	public function get(): ?string
	{
		$cache = \Cache_Global::spawn();

		if (false !== ($key = $cache->get(self::SECURE_ACCESS_KEY))) {
			return $key;
		}

		if (null !== ($key = $this->retrieveFromFrontend())) {
			$this->set($key);
		}

		return $key;
	}

	private function set(string $key): bool
	{
		$cache = \Cache_Global::spawn();
		return $cache->set(self::SECURE_ACCESS_KEY, $key);
	}

	private function retrieveFromFrontend(): ?string
	{
		if (!posix_getuid() && Apnscp::running()) {
			$pid = Apnscp::pid();
		} else {
			$gid = posix_getuid() ?: WS_GID;
			$pids = array_filter(Process::matchGroup($gid), static function (int $pid) {
				return Process::pidMatches($pid, '/usr/sbin/httpd');
			});
			$pid = array_pop($pids);
		}

		if (!$pid) {
			return null;
		}

		return Process::environment($pid)[self::SECKEY_ENV] ?? null;
	}

	private function generate(): string
	{
		$cache = \Cache_Global::spawn();

		$raw = \hash_pbkdf2('sha256', Password::generate(32), AUTH_SECRET, 20);
		$secKey = \Util_PHP::compress_hash($raw);

		$cache->set(self::SECURE_ACCESS_KEY, $secKey);
		Apnscp::running() && Apnscp::stop() && Apnscp::start();

		return $secKey;
	}

	public function reset(): void
	{
		if (APNSCPD_HEADLESS) {
			return;
		}

		$this->set($this->generate());
	}

	public function check(): bool
	{
		if (APNSCPD_HEADLESS) {
			return true;
		}

		if (null !== ($key = $this->get())) {
			$this->set($key);
			return true;
		}

		$this->reset();

		return false;
	}
}