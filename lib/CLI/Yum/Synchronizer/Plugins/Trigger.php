<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins;

	abstract class Trigger implements PluginInterface
	{
		use LimitPackage;

		/**
		 * Package is installed
		 *
		 * @param string $package
		 * @return bool
		 */
		public function install(string $package): bool
		{
			return true;
		}

		/**
		 * Package is removed
		 *
		 * @param string $package
		 * @return bool
		 */
		public function remove(string $package): bool
		{
			return true;
		}

		/**
		 * Package is updated
		 *
		 * @param string $package
		 * @return bool
		 */
		public function update(string $package): bool
		{
			return true;
		}

	}