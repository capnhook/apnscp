<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	/**
	 * Class Postgresql10Libs
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class Postgresql10 extends PostgresqlAnyVersion
	{
		/**
		 * @var array
		 */
		protected $alternatives = [
			[
				'name'     => 'pgsql-psql',
				'src'      => '/usr/bin/psql',
				'dest'     => '/usr/pgsql-{pg_version}/bin/psql',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-clusterdb',
				'src'      => '/usr/bin/clusterdb',
				'dest'     => '/usr/pgsql-{pg_version}/bin/clusterdb',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-createdb',
				'src'      => '/usr/bin/createdb',
				'dest'     => '/usr/pgsql-{pg_version}/bin/createdb',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-createuser',
				'src'      => '/usr/bin/createuser',
				'dest'     => '/usr/pgsql-{pg_version}/bin/createuser',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-dropdb',
				'src'      => '/usr/bin/dropdb',
				'dest'     => '/usr/pgsql-{pg_version}/bin/dropdb',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-dropuser',
				'src'      => '/usr/bin/dropuser',
				'dest'     => '/usr/pgsql-{pg_version}/bin/dropuser',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-pg_basebackup',
				'src'      => '/usr/bin/pg_basebackup',
				'dest'     => '/usr/pgsql-{pg_version}/bin/pg_basebackup',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-pg_dump',
				'src'      => '/usr/bin/pg_dump',
				'dest'     => '/usr/pgsql-{pg_version}/bin/pg_dump',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-pg_dumpall',
				'src'      => '/usr/bin/pg_dumpall',
				'dest'     => '/usr/pgsql-{pg_version}/bin/pg_dumpall',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-pg_config',
				'src'      => '/usr/bin/pg_config',
				'dest'     => '/usr/pgsql-{pg_version}/bin/pg_config',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-pg_restore',
				'src'      => '/usr/bin/pg_restore',
				'dest'     => '/usr/pgsql-{pg_version}/bin/pg_restore',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-reindexdb',
				'src'      => '/usr/bin/reindexdb',
				'dest'     => '/usr/pgsql-{pg_version}/bin/reindexdb',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-vacuumdb',
				'src'      => '/usr/bin/vacuumdb',
				'dest'     => '/usr/pgsql-{pg_version}/bin/vacuumdb',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-clusterdbman',
				'src'      => '/usr/share/man/man1/clusterdb.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/clusterdb.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-createdbman',
				'src'      => '/usr/share/man/man1/createdb.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/createdb.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-createuserman',
				'src'      => '/usr/share/man/man1/createuser.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/createuser.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-dropdbman',
				'src'      => '/usr/share/man/man1/dropdb.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/dropdb.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-dropuserman',
				'src'      => '/usr/share/man/man1/dropuser.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/dropuser.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-pg_basebackupman',
				'src'      => '/usr/share/man/man1/pg_basebackup.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/pg_basebackup.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-pg_dumpman',
				'src'      => '/usr/share/man/man1/pg_dump.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/pg_dump.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-pg_dumpallman',
				'src'      => '/usr/share/man/man1/pg_dumpall.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/pg_dumpall.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-pg_restoreman',
				'src'      => '/usr/share/man/man1/pg_restore.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/pg_restore.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-psqlman',
				'src'      => '/usr/share/man/man1/psql.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/psql.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-reindexdbman',
				'src'      => '/usr/share/man/man1/reindexdb.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/reindexdb.1',
				'priority' => 1000
			],
			[
				'name'     => 'pgsql-vacuumdbman',
				'src'      => '/usr/share/man/man1/vacuumdb.1',
				'dest'     => '/usr/pgsql-{pg_version}/share/man/man1/vacuumdb.1',
				'priority' => 1000
			],

		];
	}