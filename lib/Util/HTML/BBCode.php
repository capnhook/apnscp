<?php
	require_once('Util/HTML/BBCode/Parser.php');

	class Util_HTML_BBCode
	{
		private static $_filters = array(
			'/<a.*?href=\"(.*?)\".*?>(.*?)<\/a>/'                               => '[url=$1]$2[/url]',
			'/<font.*?color=\"(.*?)\".*?class=\"codeStyle\".*?>(.*?)<\/font>/'  => '[code][color=$1]$2[/color][/code]',
			'/<font.*?color=\"(.*?)\".*?class=\"quoteStyle\".*?>(.*?)<\/font>/' => '[quote][color=$1]$2[/color][/quote]',
			'/<font.*?class=\"codeStyle\".*?color=\"(.*?)\".*?>(.*?)<\/font>/'  => '[code][color=$1]$2[/color][/code]',
			'/<font.*?class=\"quoteStyle\".*?color=\"(.*?)\".*?>(.*?)<\/font>/' => '[quote][color=$1]$2[/color][/quote]',
			'/<span style=\"color: ?(.*?)\">(.*?)<\/span>/'                     => '[color=$1]$2[/color]',
			'/<font.*?color=\"(.*?)\".*?>(.*?)<\/font>/'                        => '[color=$1]$2[/color]',
			'/<span style=\"font-size:(.*?)\">(.*?)<\/span>/'                   => '[size=$1]$2[/size]',
			'/<font>(.*?)<\/font>/'                                             => '$1',
			'/<img.*?src=\"(.*?)\".*?\/>/'                                      => '[img]$1[/img]',
			'/<span class=\"codeStyle\">(.*?)<\/span>/'                         => '[code]$1[/code]',
			'/<span class=\"quoteStyle\">(.*?)<\/span>/'                        => '[quote]$1[/quote]',
			'/<span(?:[^>]*)>(.*?)<\/span>/sm'                                  => '$1',
			'/<strong class=\"codeStyle\">(.*?)<\/strong>/'                     => '[code][b]$1[/b][/code]',
			'/<strong class=\"quoteStyle\">(.*?)<\/strong>/'                    => '[quote][b]$1[/b][/quote]',
			'/<em class=\"codeStyle\">(.*?)<\/em>/'                             => '[code][i]$1[/i][/code]',
			'/<em class=\"quoteStyle\">(.*?)<\/em>/'                            => '[quote][i]$1[/i][/quote]',
			'/<u class=\"codeStyle\">(.*?)<\/u>/'                               => '[code][u]$1[/u][/code]',
			'/<u class=\"quoteStyle\">(.*?)<\/u>/'                              => '[quote][u]$1[/u][/quote]',
			'/<\/(strong|b)>/'                                                  => '[/b]',
			'/<(strong|b)>/'                                                    => '[b]',
			'/<\/(em|i)>/'                                                      => '[/i]',
			'/<(em|i)>/'                                                        => '[i]',
			'/<\/u>/'                                                           => '[/u]',
			'/<span style=\"text-decoration: ?underline;\">(.*?)<\/span>/'      => '[u]$1[/u]',
			'/<u>/'                                                             => '[u]',
			'/<blockquote[^>]*>/'                                               => '[quote]',
			'/<\/blockquote>/'                                                  => '[/quote]',
			'/<br \/>/'                                                         => "\n",
			'/<br\/>/'                                                          => "\n",
			'/<div[^>]*>/'                                                      => '',
			'/<\/div>/'                                                         => "\n",
			'/<br>/'                                                            => "\n",
			'/<p>/'                                                             => '',
			'/<\/p>/'                                                           => "\n",
			'/&nbsp;/'                                                          => ' ',
			'/&quot;/'                                                          => '"',
			'/&lt;/'                                                            => '<',
			'/&gt;/'                                                            => '>',
			'/&amp;/'                                                           => '&',
		);

		public static function clean2HTML($in)
		{
			return self::BB2HTML(self::stripHTML($in));
		}

		public static function BB2HTML($in)
		{
			$parser = new Util_HTML_BBCode_Parser();
			$parser->addFilters(array('basic', 'email', 'extended', 'images', 'ticket', 'links', 'lists'));

			return $parser->qparse($in);
		}

		public static function stripHTML($in)
		{
			return strip_tags($in, '<br>');
		}

		public static function sanitizeBBCode($in)
		{
			$out = self::HTML2BB($in);
			$out = self::stripHTML($out);

			return $out;
		}

		/**
		 * Convert acceptable HTML tags to BBCode
		 *
		 * @TODO optimize!
		 * @param  string $in
		 * @return string
		 */
		public static function HTML2BB($in)
		{
			return preg_replace(array_keys(self::$_filters), array_values(self::$_filters), $in);
		}
	}