<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */


namespace Util\Process;

use Util\Process\Unshared\Contracts\Unshareable;

class Unshare {
	use \NamespaceUtilitiesTrait;

	protected $resources = [];

	/**
	 * @var array
	 */
	protected $namespaces = [];
	/**
	 * @var string
	 */
	protected $command = '/bin/sh';

	public function __construct()
	{

	}

	/**
	 * Command in unshare context
	 *
	 * @param string $command
	 */
	public function setCommand(string $command) {
		$this->command = $command;
	}

	public function add(Unshareable $item) {
		$this->resources[] = $item;
		$namespace = strtolower(static::getBaseClassName(\get_class($item)));
		$this->namespaces[$namespace] = $item->getInitializer();
		return $this;
	}

	public function __toString(): string
	{
		if (!$this->resources) {
			return '';
		}

		$cmd = '/usr/bin/unshare ' . implode(' ', array_key_map(static function($ns, $initializer) {
			return '--'  . $ns . ($initializer ? ' ' . $initializer : '');
		}, $this->namespaces));
		$commands = array_filter(array_map('\strval', $this->resources));
		$commands[] = 'exec ' . $this->command;
		$cmd .= ' /bin/sh -c ';
		$cmd .= escapeshellarg(implode(';', $commands));
		return $cmd;
	}

	/**
	 * Get unshare namespaces
	 *
	 * @return array
	 */
	public function getNamespaces(): array {
		return array_keys($this->namespaces);
	}
}
