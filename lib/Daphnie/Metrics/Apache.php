<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Daphnie\Metrics;

use Daphnie\Contracts\MetricProvider;
use Daphnie\Metric;
use Daphnie\MutableMetricTrait;

class Apache extends Metric implements  MetricProvider
{
	use MutableMetricTrait;

	public const ATTRVAL_MAP = [
		'req'  => 'total',
		'idle' => 'idle',
		'busy' => 'busy',
		'rate' => 'reqsec',
		'bw'   => 'bw'
	];

	private const ATTR_BINDINGS = [
		'req' => [
			'label' => 'Total requests',
			'type'  => MetricProvider::TYPE_MONOTONIC
		],
		'idle' => [
			'label' => 'Idle workers',
			'type'  => MetricProvider::TYPE_VALUE
		],
		'busy' => [
			'label' => 'Busy workers',
			'type'  => MetricProvider::TYPE_VALUE
		],
		'rate' => [
			'label' => 'Requests/second',
			'type'  => MetricProvider::TYPE_VALUE
		],
		'bw'  => [
			'label' => 'Bandwidth',
			'unit'  => 'B/sec',
			'type'  => MetricProvider::TYPE_VALUE
		]
	];
}
