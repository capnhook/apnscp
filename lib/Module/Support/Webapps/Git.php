<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */

namespace Module\Support\Webapps;

use Module\Support\Webapps;
use Module\Support\Webapps\MetaManager\Meta;
use Opcenter\Provisioning\ConfigurationWriter;
use Opcenter\SiteConfiguration;

class Git {
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;
	use \FilesystemPathTrait;
	/**
	 * @var string
	 */
	protected $path;
	/**
	 * @var Meta
	 */
	protected $meta;
	/**
	 * @var \apnscpFunctionInterceptor
	 */
	protected $gitWrapper;

	protected function __construct(string $approot, Meta $meta)
	{
		$stat = $this->file_stat($approot);
		$owner = array_get($stat, 'owner', $this->getAuthContext()->username);
		if (is_int($owner)) {
			warn("Unknown UID %(uid)d detected for `%(path)s', defaulting to %(user)s", [
				'uid' => $owner,
				'path' => $approot,
				'user' => $this->getAuthContext()->username
			]);
			$owner = $this->getAuthContext()->username;
		}
		$this->gitWrapper = $this->getApnscpFunctionInterceptor();
		if ($owner !== $this->getAuthContext()->username &&
			$stat['uid'] >= \a23r::get_class_from_module('user')::MIN_UID)
		{
			$this->gitWrapper = \apnscpFunctionInterceptor::factory(
				\Auth::context($owner, $this->getAuthContext()->site)
			);
		}

		$this->path = $approot;
		$this->meta = $meta;
	}

	/**
	 * git present for path
	 *
	 * @return bool
	 */
	public function enabled(): bool
	{
		return array_get($this->meta->getOptions(), 'git', true) &&
			file_exists($this->domain_fs_path($this->path . '/.git'));
	}

	/**
	 * Remove git backing
	 *
	 * @return bool
	 */
	public function remove(): bool
	{
		$this->cleanDatabase();
		$afi = $this->getApnscpFunctionInterceptor();

		if (!$afi->file_delete($this->path . '/.git', true)) {
			return false;
		}

		$afi->file_delete($this->path . '/.gitignore');
		$this->meta->setOption('git', false);

		return true;
	}

	/**
	 * Clean database staging
	 */
	public function cleanDatabase(): void
	{
		$afi = $this->getApnscpFunctionInterceptor();
		foreach (['.db-credentials.json', '.db-dump.sql'] as $file) {
			$path = $this->path . '/' . $file;
			if ($afi->file_exists($path)) {
				$afi->file_delete($path);
			}
		}
	}

	/**
	 * Create git storage
	 *
	 * @return bool
	 */
	public function createRepository(): bool
	{
		return (bool)\serial(function () {
			// @TODO handle different user ownership
			$this->initializeGit();
			$this->meta->setOption('git', true);

			return true;
		});
	}

	/**
	 * Initialize bare repository
	 *
	 * @return bool
	 */
	protected function initializeGit(): bool
	{
		$this->gitWrapper->git_init($this->path, false);
		$this->file_chmod($this->path . '/.git', '0700');

		$this->file_put_file_contents(
			$this->path . '/.git/.htaccess',
			'Require all denied'
		);

		if (!$this->file_exists($this->path . '/.gitignore')) {
			$svc = SiteConfiguration::shallow($this->getAuthContext());
			$template = (string)(new ConfigurationWriter('webapps.gitignore', $svc))->compile([
				'type' => array_get($this->meta, 'type', null)
			]);

			$this->file_put_file_contents(
				$this->path . '/.gitignore',
				$template
			);
		}

		return true;
	}

	/**
	 * Process webapp snapshot
	 *
	 * @param string $comment
	 * @return bool
	 */
	public function snapshot(string $comment): ?string
	{
		return \serial(function () use ($comment) {
			$this->stageDatabase();

			if (!$this->gitWrapper->git_add($this->path, null)) {
				return null;
			}

			return $this->gitWrapper->git_commit($this->path, $comment);
		});
	}

	/**
	 * Get last snapshot commit
	 *
	 * @return string|null
	 */
	public function lastSnapshot(): ?string
	{
		$last = $this->commits();
		return array_get(array_pop($last), 'hash');
	}

	public function commits(): array
	{
		return $this->gitWrapper->git_list_commits($this->path);
	}

	/**
	 * Get class mapping
	 *
	 * @return string|null
	 */
	protected function getClassMapping(): ?string {
		return Webapps\App\Loader::fromDocroot(
			array_get($this->meta, 'type'),
			$this->meta->getDocumentRoot(),
			$this->getAuthContext()
		)->getClassMapping();
	}

	/**
	 * Get hostname
	 *
	 * @return string
	 */
	protected function getHostname(): string {
		return array_get($this->meta, 'hostname');
	}

	/**
	 * Get path
	 *
	 * @return string
	 */
	protected function getPath(): string {
		return array_get($this->meta, 'path', '');
	}

	/**
	 * Stage web app database for snapshot
	 *
	 * @return bool
	 */
	protected function stageDatabase(): bool
	{
		$func = $this->getClassMapping() ?? 'webapp';

		// has database
		$config = $this->{$func . '_db_config'}($this->getHostname(), $this->getPath());
		if (!$config) {
			return true;
		}

		$type = array_get($config, 'type', 'mysql');
		if (!$db = array_get($config, 'db')) {
			return error('Failed to fetch database name');
		}

		$this->cleanDatabase();

		$this->gitWrapper->file_touch($this->path . '/.db-dump.sql');
		$this->file_set_acls($this->path . '/.db-dump.sql', [$this->getAuthContext()->username => 'rw']);
		$this->{$type . '_export'}(
			array_get($config, 'db'),
			$this->path . '/.db-dump.sql'
		);


		$this->gitWrapper->file_create_file($this->path . '/.db-credentials.json', 0600);

		return $this->file_put_file_contents(
			$this->path . '/.db-credentials.json',
			json_encode($config)
		);
	}

	/**
	 * Get head commit
	 *
	 * @return string
	 */
	public function head(): string
	{
		return $this->gitWrapper->git_head($this->path);
	}

	/**
	 * Discard commits promoting $whence to head
	 *
	 * @param string $whence
	 * @param string $to
	 * @return bool
	 */
	public function discard(string $whence, string $to = null): bool
	{
		if (!$to) {
			return $this->gitWrapper->git_reset($this->path, $whence, true);
		}

		return error('Not implemented');
	}

	/**
	 * Restore from a snapshot
	 *
	 * @param string|null $commit
	 * @param bool        $prune  prune history after rollback
	 * @return bool
	 */
	public function restore(?string $commit, bool $prune = true): bool
	{
		return (bool)\serial(function () use ($commit, $prune) {
			$this->gitWrapper->git_reset($this->path, $commit, $prune);
			if (!$func = $this->getClassMapping()) {
				return true;
			}
			// has database
			$config = $this->{$func . '_db_config'}($this->getHostname(), $this->getPath());
			if (!$config) {
				return true;
			}

			$type = array_get($config, 'type', 'mysql');
			if (!$db = array_get($config, 'db')) {
				return error('Failed to fetch database name');
			}

			$this->{$type . '_empty_database'}($db);
			$this->{$type . '_import'}($db, $this->path . '/.db-dump.sql');

			// @todo reconfigure application to use credentials
			return true;
		});
	}
}
