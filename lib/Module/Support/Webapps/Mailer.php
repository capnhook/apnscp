<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);


	namespace Module\Support\Webapps;

	use Illuminate\Mail\Mailable;

	class Mailer extends Mailable
	{
		public $view;
		protected $viewdata;
		protected $appName = 'App';

		public function __construct(string $view, array $viewdata = [])
		{
			$this->view = $view;
			$this->viewdata = $viewdata;
		}

		public function setAppName(string $name)
		{
			$this->appName = $name;

			return $this;
		}

		public function build()
		{
			$view = $this->view[0] === '@' ? $this->view : 'email.webapps.' . $this->view;
			return $this->markdown($view,
				$this->viewdata)->subject($this->appName . ' Installed');
		}
	}