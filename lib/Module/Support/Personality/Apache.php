<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support\Personality;

	class Apache extends Helpers\Base
	{

		const PRIORITY = 10;
		const NAME = 'apache';
		protected $_tokens = array(
			'RewriteEngine'  => array(
				'help' => 'Enable or disable rewrite processing.'
			),
			'RewriteRule'    => array(
				'help' => 'Map a URL request to the new resource.'
			),
			'RewriteCond'    => array(
				'help' => 'Place before RewriteRule. Trigger RewriteRule if this condition matches.'
			),
			'DirectorySlash' => array(
				'help' => 'Automatically append a trailing directory slash to requests if no slash provided.'
			),
			'SetEnvIf'       => array(
				'help' => 'Set an ENVIRONMENT variable if a particular condition is met.'
			),
			'FileETag'       => array(
				'help' => 'Cache static files based upon the following conditions.'
			)
		);
		private $_flagOnly = array('directoryslash', 'rewriteengine');

		public function test($directive, $val = null)
		{
			$dirtest = strtolower($directive);
			$testval = strtolower($val);
			if (\in_array($dirtest, $this->_flagOnly)) {
				return $this->_testFlag($directive, $val);
			}

		}
	}