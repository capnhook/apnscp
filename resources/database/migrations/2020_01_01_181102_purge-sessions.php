<?php

	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PurgeSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    	if (Schema::connection('mysql')->hasTable('session_information')) {
			DB::connection('mysql')->getPdo()->exec('TRUNCATE TABLE session_information');
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
