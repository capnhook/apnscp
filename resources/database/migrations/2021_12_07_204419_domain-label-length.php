<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DomainLabelLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		$db = \Illuminate\Support\Facades\DB::connection('pgsql')->getPdo();
		$db->beginTransaction();
		$db->exec("ALTER TABLE siteinfo ALTER COLUMN domain TYPE TEXT");
		$db->exec("ALTER TABLE siteinfo ADD CHECK ( char_length(domain) <= 253 )");
		$db->exec("ALTER TABLE domain_lookup ALTER COLUMN domain TYPE TEXT");
		$db->exec("ALTER TABLE domain_lookup ADD CHECK ( char_length(domain) <= 253 )");
		$db->exec("ALTER TABLE email_lookup ALTER COLUMN domain TYPE TEXT");
		$db->exec("ALTER TABLE email_lookup ADD CHECK ( char_length(domain) <= 253 )");
		$db->exec("ALTER TABLE sessions ALTER COLUMN domain TYPE TEXT");
		$db->exec("ALTER TABLE sessions ADD CHECK ( char_length(domain) <= 253 )");
		$db->commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
