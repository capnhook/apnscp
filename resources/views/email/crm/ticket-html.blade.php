@extends("apnscp-template")
@section("title", "Support Incident")
@section("body")
<table class="" width="100%" cellpadding="0" cellspacing="0">

    @if ($state === 'new')
        @if ($sender === 'admin' && $recipient === 'site')
            <tr>
                <td class="alert alert-warning"
                    style="color:#fff;font-weight:500;padding:20px;text-align:center;background:#ff9f00;">
                    Attention required: A ticket has been opened on your account.
                </td>
            </tr>
        @else
            <tr>
                <td class="alert alert-info"
                    style="color:#fff;font-weight:500;padding:20px;text-align:center;background:#9d9d9d;">
                    A new ticket has been opened.
                </td>
            </tr>
        @endif
    @elseif ($state === 'close')
    <tr>
        <td class="alert alert-good"
            style="color:#fff;font-weight:500;padding:20px;text-align:center;background:#68b90f;">
            Ticket has been resolved.
        </td>
    </tr>
    @endif
    <tr>
        <td style="">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 0 none; ">
                @if ($recipient === 'admin')
                <tr>
                    <td class="">
                        <ul class="inline">
                            <li class="meta">
                                <b>DOMAIN:</b> {{ $domain }}
                            </li>
                            <li class="meta">
                                <b>SITE ID:</b> {{ $siteid }}
                            </li>
                            <li class="meta">
                                <b>SERVER:</b> {{ $server }}
                            </li>
                            <li class="meta">
                                <b>ADMIN:</b> {{ $username }}
                            </li>
                            <li class="meta">
                                <b>PACKAGE:</b> {{ $package }}
                            </li>
                            @if ($invoice)
                            <li class="meta">
                                <b>INVOICE:</b> {{ $invoice }}
                            </li>
                            @endif
                        </ul>

                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="content-block" style="padding:0 0 20px;">
                        <hr/>
                    </td>
                </tr>
                @elseif ($recipient === "site")
                <tr>
                    <td colspan="3">
                        <h2>{{ $domain }}</h2>
                    </td>
                </tr>
                @endif
                <tr>
                    <td class="content-block" style="padding:0 0 20px;">
                        <p>
                            {!! $data !!}
                        </p>
                    </td>
                </tr>
                @if ($state !== 'new' && !empty($inreplyto))
                <tr>
                    <td class="content-block" style="padding:0 0 20px;">
                        <h3>In response to:</h3>
                    </td>
                </tr>
                <tr>
                    <td class="content-block context" style="padding:0 0 20px;color:#999;">
                        <p>
                            {!! $inreplyto !!}
                        <p>
                    </td>
                </tr>
                @endif
                @if ($state !== 'close')
                <tr>
                    <td class="content-block" style="padding:0 0 20px;">
                        <a href="{!! $url !!}"
                           style="text-decoration:none;color:#FFF;background-color:#993950;border:solid #993950;border-width:10px 20px;line-height:2;font-weight:bold;text-align:center;cursor:pointer;display:inline-block;border-radius:5px;text-transform:capitalize;"
                           class="btn-primary">Respond to Ticket</a> &ndash; or just reply to this
                        email!
                    </td>
                </tr>
                @endif
            </table>
        </td>
    </tr>
</table>
@endsection

@section('footer')
    <table width="100%">
        <tr>
            <td class="aligncenter content-block" style="padding:0 0 20px;">
                This has been sent to all listed addresses on the ticket. To make changes,
                open this <a href="{!! $url !!}">ticket</a> in the control panel, then edit the EMAIL
                field to attach
                or remove additional contacts.
            </td>
        </tr>
    </table>
    @parent
@endsection