<div id="ui-notifications"
     class="collapse ui-flyout @if (!\Preferences::get(Page_Renderer::THEME_SWAP_BUTTONS)) ui-flyout-right @endif"
     aria-expanded="false">
	<div id="ui-notification-sink">
		<div class="row">
			<div class="col-12 text-center">
				<img src="/images/notifications/empty.svg" alt="No notifications" style="max-height: 128px;"/>
				<h6 class="font-weight-light mt-3">{{ _("No notifications") }}</h6>
			</div>
		</div>
	</div>
</div>