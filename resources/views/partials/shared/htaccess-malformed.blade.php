<div class="alert alert-danger col-12">
	<h5 class="mt-3 text-danger"><i class="fa fa-exclamation-triangle"></i> Malformed .htaccess file</h5>
	<p class="">
		Your .htaccess file in <b>{{ $file }}</b> is bad. This can happen from inconsistent or improperly
		balanced blocks.
		For example if a block opens with <code>&lt;IfModule&gt;</code>, then it should close with <code>&lt;/IfModule&gt;</code>,
		not
		<code>&lt;/ifModule&gt;</code> nor <code>&lt;/ifmodule&gt;</code> and most certainly never <code>&lt;/ifMoDUle&gt;</code>.
		<br/><br/>
		Here's the error we found. Try correcting the offending line and try again.<br/>
		@foreach (\Error_Reporter::get_errors() as $e)
			- {{ $e }} <br/>
		@endforeach
		<a href="/apps/filemanager?f={{ $file }}"
		   class="mt-3 ui-action-switch-app ui-action-label ui-action btn btn-primary">Edit .htaccess</a>
	</p>
</div>