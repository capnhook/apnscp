#!/usr/bin/env apnscp_php
<?php
    include(dirname(__FILE__).'/../../lib/CLI/cmd.php');
    Error_Reporter::set_verbose(1);
    $args = cli\parse();
    $afi  = cli\get_instance();
    $argv = $_SERVER['argv'];
    if ($_SERVER['argc'] < 2) {
        usage();
    }
    $sync = CLI\Yum\Synchronizer::factory($_SERVER['argv']);
    if (is_bool($sync)) {
        // let factory take care of modes
        if (!$sync) {
            usage();
        }
        // recognized no-op
        exit(0);
    }
    try {
	    $ret = $sync->run();
    } catch (\Throwable $e) {
    	\Error_Reporter::handle_exception($e);
    	exit(1);
    }

    if (Error_Reporter::is_error()) {
        exit(1);
    }
    exit(0);

    function usage() {
        echo "Usage: " . basename($_SERVER['argv'][0]) . " <mode> <args>\n" .
            "  mode args\t\t : description\n" .
            "  " . str_repeat("-", 72) . "\n" .
            "  clean\t\t\t : remove orphaned packages from internal DB\n" .
            "  generate\t\t : create package list from installed packages\n" .
            "  install [-d, --force] pkg svc\t : install package <pkg> into FST service <svc>\n" .
			"  depends pkg\t\t : enumerate package dependencies\n" .
            "  obsoleting pkg\t : package <pkg> supersession \n\t\t\t   " .
                "(obsoleted queried from package)\n" .
            "  provides pkg\t\t : get filelist generated from generate of \n\t\t\t   " .
                "package <pkg> installed in FST\n" .
            "  remove pkg\t\t : remove installed package <pkg> from FST\n" .
            "  resync [--force]\t : update FST packages with system packages \n\t\t\t   " .
                "on mismatch or do all (--force)\n" .
            "  update pkg [ver [rel]] : update FST package <pkg> with system \n\t\t\t   " .
                "version <ver> (<ver>/<rel> rpm query if absent)\n";
        exit(1);
    }