@component('partials.generic-service', ['service' => "FTP", 'auth' => $auth])
	@slot("extra")
		<tfoot>
		<tr>
			<th colspan="2">Configuration Profiles</th>
		</tr>
		<tr>
			<td class="text-center">
				<a class="" href="{{ \HTML_Kit::page_url() }}/download/ftp/filezilla">
					@includeWhen($logo = array_get($vars, 'ftp-file-ext.filezilla.logo'), 'partials.profile-logo', ['src' => $logo])
					Filezilla
				</a>
			</td>
			<td class="text-center">
				<a class="" href="{{ \HTML_Kit::page_url() }}/download/ftp/cyberduck">
					@includeWhen($logo = array_get($vars, 'ftp-file-ext.cyberduck.logo'), 'partials.profile-logo', ['src' => $logo])
					Cyberduck
				</a>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<h6>Notes</h6>
				FTP server supports explicit SSL (Auth TLS/STARTTLS).
				@if ($auth->hasSsh())
					Alternatively, SFTP may be used which relies on SSH to encrypt traffic.
				@endif
				Configuration Profiles provide turnkey setup with the respective software.
			</td>
		</tr>
		</tfoot>
	@endslot
@endcomponent