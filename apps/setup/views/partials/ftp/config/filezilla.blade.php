{{--
	@var \apps\setup\models\Authentication $auth
--}}
@php
 echo '<?xml version = "1.0" encoding = "UTF-8"?>';
@endphp
<FileZilla3 version="3.56.0" platform="windows">
	<Servers>
		<Server>
			<Host>{{ $auth->hostname('ftp', true) }}</Host>
			<Port>21</Port>
			<Protocol>0</Protocol>
			<Type>0</Type>
			<User>{{ $auth->username() }}{{ '@' }}{{ $auth->hostname('ftp') }}</User>
			<Logontype>3</Logontype>
			<PasvMode>MODE_DEFAULT</PasvMode>
			<EncodingType>Auto</EncodingType>
			<BypassProxy>0</BypassProxy>
			<Name>{{ $auth->hostname('ftp', true) }}</Name>
			<SyncBrowsing>0</SyncBrowsing>
			<DirectoryComparison>0</DirectoryComparison>
		</Server>
	</Servers>
</FileZilla3>
