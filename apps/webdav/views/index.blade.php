<!-- main object data goes here... -->
<form method="post" action="/apps/webdav.php">
	<div class="row">
		<div class="col-12 col-md-6 col-lg-5 col-xl-6 float-right">
			<h3>Manage Locations</h3>
			<table width="100%" class="table">
				<thead>
				<tr>
					<th class="directory">
						Directory
					</th>
					<th class="provider">
						Provider
					</th>
					<th class="actions text-center">
						Actions
					</th>
				</tr>
				</thead>
				@foreach($Page->get_dav_locations() as $dav_path)
				<tr>
					<td class="directory">
						{{ $dav_path['path'] }}
					</td>
					<td class="provider">
						{{ $dav_path['provider'] }}
					</td>
					<td class="actions" align="center">
						<button type="submit" alt="Delete" name="[{{ $dav_path['path'] }}]" value="1"
						        class="warn btn btn-secondary ui-action ui-action-delete ui-action-label"
						        OnClick="return confirm('Are you sure you would like to remove {{ $dav_path['path'] }}?')">
							Delete
						</button>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
		<div class="col-12 col-md-6 float-left">
			<h3>Add WebDAV Location</h3>
			<div class="form-inline form-group row">
				<div class="col-12 col-sm-8 col-lg-9">
					<label class="block">Folder</label>

					<span class="btn-group">
						<input type="text" name="new_directory" id="webdav_dir" value="/var/www/html" value=""
						       class="form-control"/>
						<button id="browser" type="button" class="btn btn-secondary" name="browse"><i
									class="fa fa-folder-open"></i>Browse</button>
					</span>
				</div>
				<div class="col-6 col-sm-5 col-lg-3">
					<label class="block">
						Provider
					</label>
					<select name="provider" class="form-control custom-select" id="provider">
						<option value="dav">DAV</option>
						@if ($Page->svn_enabled())
							<option value="svn">svn</option>
						@endif
					</select>
				</div>
				<div class="mt-3 col-12 col-lg-6">
					<input type="submit" name="add" class="btn btn-primary" value="Add WebDAV"/>
				</div>
				<div class="mt-3 col-12 col-lg-6">
					<button type="submit" name="synchronize"
					        class="ui-action btn btn-secondary ui-action-activate ui-action-label"
					        value="Reload Configuration">
						Reload Configuration
					</button>
				</div>
			</div>
		</div>
	</div>
</form>
