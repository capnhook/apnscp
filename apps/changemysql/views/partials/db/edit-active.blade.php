<div class="btn-group">
	<button type="submit" value="Save" class="grants-save btn btn-primary" name="Save_Grants">
		Save
	</button>
	<button class="btn btn-secondary dropdown-toggle ui-action ui-action-advanced ui-action-label"
	        data-toggle="dropdown" aria-expanded="false">
		<span class="sr-only">Toggle Advanced</span>
	</button>
	<div class="dropdown-menu">
		<button name="export[{{ $mydb }}]" value=""
		        class="ui-action-download btn-block dropdown-item ui-action-label btn  ui-action">
			Export
		</button>
		<button name="import[{{ $mydb }}]" data-db="{{ $mydb }}" value=""
		        class="ui-action-restore btn-block dropdown-item ui-action-label btn  ui-action">
			Restore from Backup
		</button>
		<button name="snapshot[{{ $mydb }}]" data-db="{{ $mydb }}" value=""
		        class="ui-action-snapshot btn-block dropdown-item ui-action-label btn  ui-action">
			Snapshot
		</button>
		<div class="dropdown-divider"></div>
		<button type="submit"
		        class="ui-action-ban btn-block dropdown-item ui-action-label btn  ui-action"
		        name="repair[{{ $mydb }}]">
			<i class="fa fa-heartbeat"></i>
			Repair Tables
		</button>
		<div class="dropdown-divider"></div>
		<button type="submit"
		        class="ui-action-ban btn-block dropdown-item ui-action-label btn  warn ui-action"
		        name="empty[{{ $mydb }}]"
		        onClick="return confirm('Are you sure you want to empty all tables in the database {{ $mydb }}?');">
			<i class="fa fa-trash-o"></i>
			Empty Tables
		</button>

		<button type="submit"
		        class="ui-action-delete btn-block dropdown-item ui-action-label btn  warn ui-action"
		        name="drop[{{ $mydb }}]"
		        onClick="return confirm('Are you sure you want to drop the database {{ $mydb }}?');">
			Delete Database
		</button>
	</div>
</div>