@includeWhen(!$enabled, 'disabled')
@includeWhen($enabled, 'partials.toolbar')
@includeWhen($mode === 'list', 'list', ['users' => $Page->getUsers()])
@includeWhen($mode !== 'list', 'add')