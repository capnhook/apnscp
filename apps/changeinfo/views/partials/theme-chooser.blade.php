<label class="form-control-static d-block">
	Active Theme
	@php $myTheme = $Page->getActiveTheme(); @endphp
	<select name="theme" class="custom-select form-control">
		@foreach ($Page->getThemes() as $theme)
			<option value="{{ $theme }}" @if ($theme === $myTheme) selected="SELECTED" @endif />
			{{ ucwords(str_replace('_', ' ', $theme)) }}
			</option>
		@endforeach
		@if (\Frontend\Css\StyleManager::allowCustom() && \Frontend\Css\StyleManager::exists())
			@php
				$active = \Frontend\Css\StyleManager::isActive();
				$custom = \Frontend\Css\StyleManager::getTheme();
			@endphp
			<option id="load-theme-dialog2" @if ($active) selected="SELECTED" @endif value="{{ $custom }}">
				Use My Theme
			</option>

		@endif
	</select>

	@if (STYLE_ALLOW_CUSTOM)
		<div class="text-right">
			<button class="btn btn-secondary mt-3" type="button" id="load-theme-dialog">
				@if (\Frontend\Css\StyleManager::exists())
					<i class="fa fa-paint-brush"></i> Replace Custom Theme
				@else
					<i class="fa fa-paint-brush"></i> Add Custom Theme
				@endif
			</button>
		</div>
	@endif
</label>