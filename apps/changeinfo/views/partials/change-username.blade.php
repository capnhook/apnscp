<label class="form-control-static">
	Username
</label>

<fieldset class="form-group">
	{{ $_SESSION['username'] }}
	<button type="button" class="btn btn-secondary change" id="change-username">
		<i class="ui-action ui-action-edit"></i>
		Change
	</button>
</fieldset>

<div class="rules" id="username-rules">
	<h3>Important Rules</h3>
	<ol class="rules">
		<li>This username cannot be in use by any other user on your account.
		<li>This username cannot be in use by as a master username by another account on this server.
		<li>Any database configurations that reference <em>{{ \UCard::get()->getUser() }}</em> must be
			changed.
		<li>Upon changing username, you will be automatically logged out. Login with your new username.
	</ol>
	<label class="custom-control custom-checkbox mb-0">
		<input type="checkbox" class="custom-control-input"
		       name="delfiles" value="agree"/>
		<span class="custom-control-indicator"></span>
		I understand the rules!
	</label>
</div>