<h3>
	Deliver threshold
</h3>
<p class="">
	Mail that scores above this threshold will be automatically deleted. No delivery will occur and no notification
	generated. Mail that scores below this threshold but above your spam threshold
	will be delivered to your
	<a href="{{ MISC_KB_BASE }}/email/accessing-spam-folder/" class="ui-action-label ui-action ui-action-kb">Spam folder</a>.
</p>
@if (\Opcenter\Mail\Services\Rspamd::present())
<p class="text-info note">
	rspamd does not presently allow adjustment beyond the prescribed spam threshold of <b>8</b>
	and maximum deliver threshold of <b>25</b>.
</p>
@endif

<form method="POST" action="{{ Template_Engine::init()->getPathFromApp('saconfig')  }}/global">
	<div class="form-inline">
		<label class="mr-1">
			Deliver threshold
		</label>
		<div class="input-group alert-danger mr-1">
			<span class="input-group-addon alert-danger"><i class="ui-action-delete ui-action ui-action-label"></i></span>
			<input type="number" min="0" @if (\cmd('spamfilter_get_provider') === "rspamd") max="25" @endif
				class="form-control" value="{{ \cmd('spamfilter_get_delivery_threshold') }}" name="global_threshold" />
		</div>
		<button type="submit" name="submit" class="btn btn-primary ui-action warn ui-action-label ui-action-delete">
			{{ _("Set threshold") }}
		</button>
	</div>
</form>
<hr />