<?php
namespace apps\dns;
?>
<div class="row">
	<div class="form-inline col-12 col-sm-6 float-sm-left mb-3 ui-widget">
		@if (\count((array)$Page->getAvailableDomains()) > 1)
			<label class="b d-block mr-1">Active Domain</label>
			<select name="domain" id="domain-picker" class="custom-select" placeholder="{{ _('Select domain') }}" tabindex="1" @if (!$Page->getDomain()) autofocus @endif>
				@foreach ($Page->getAvailableDomains() as $domain)
					<option @if (isset($_GET['domain']) && $_GET['domain'] == $domain) SELECTED @endif value="{{ $domain }}">{{ $domain }}</option>
				@endforeach
			</select>
		@endif
	</div>

	<div class="col-12 col-sm-6">
		<div class="float-sm-right toolbox-container">
			@includeWhen($Page->getDomain(), 'partials.toolbox')
		</div>
	</div>
</div>

@include('partials.domain-info', ['domain' => $Page->getDomain()])

<form class="mt-1" method="post"
      action="{{ 'dns' . (isset($_GET['domain']) ? '?domain=' . $_GET['domain'] : '') }}">
	@includeWhen($Page->authorative, 'partials.add-record')
</form>


<form method="post" action="{{ \HTML_Kit::new_page_url_params(null, ['domain' => $Page->getDomain() ]) }}">
	@if ($Page->getDomain())
		<h3>DNS - {{ $Page->getDomain() }}</h3>
	@endif
	@includeWhen($Page->authorative, 'partials.list-records')
</form>
@if ($Page->getDomain() && !$Page->dns_verified($Page->getDomain()))
	@include('partials.zone-verification')
@else
	@includeWhen($Page->authorative, 'partials.clone-domain')
	@includeWhen(!$Page->authorative, 'partials.zone-not-authoritative')
@endif