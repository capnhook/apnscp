@extends('partials.scope-scene')
@push('after-render')
	<script>
		registerEvent('click', 'a.section-list', function (e) {
			$(this).siblings('a.active').removeClass('active');
			e.preventDefault();
			$(this).tab('show')
		});
		registerEvent('focus', 'input', function () {
			$(this).data('original', $(this).val());
		}).registerEvent('blur', 'input', function () {
			var type = this.type.toUpperCase();
			if (type !== 'NUMBER' && type !== 'TEXT') {
				// double firing
				return true;
			}
			if ($(this).val() === $(this).data('original')) {
				$(this).removeData('original');
				return;
			}
			enqueueConfiguration(this.name, this.value);
			return true;
		}).registerEvent('keydown', 'input', function (e) {
			var keyCode = $.ui.keyCode;
			if (e.keyCode === keyCode.ESCAPE) {
				$(this).val($(this).data('original'));
				return $(this).blur();
			} else if (e.keyCode === keyCode.ENTER) {
				return $(this).blur();
			}
			return true;
		}).registerEvent('change', ':checkbox', function () {
			enqueueConfiguration(this.name, !!$(this).prop('checked'));
			return true;
		}).registerEvent('change', '#confirmChanges', function () {
			$('#scopeScene :submit[name="scope"]').prop('disabled', !$(this).prop('checked'));
		});

		$('#scopeScene :submit[name="scope"]').prop('disabled', true);

		function enqueueConfiguration(varname, value) {
			var section = varname.substring(0, varname.indexOf('['));
			var option = varname.substring(varname.indexOf('[')+1, varname.length-1);
			return apnscp.post({args: [section, option, value]}, 'apply/' + globalScopeName).done(function () {
				apnscp.addMessage('[' + section + '] => ' + option + ' changed');
			})
		}
	</script>
@endpush