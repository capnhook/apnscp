<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

	namespace apps\scopes\models\scopes\Php;
	use apps\scopes\models\Scope;
	use apps\scopes\RenderAsHtmlTrait;
	use Opcenter\Admin\Settings\Php\Version;

	class Multi extends Scope {
		use RenderAsHtmlTrait;

		public function __construct(string $name)
		{
			parent::__construct($name);
			foreach ((new Version())->getValues() as $v) {
				if (!isset($this->value[$v])) {
					$this->value[$v] = 'disabled';
				}
			}
			ksort($this->value);
		}

		public function getHelp()
		{
			$help = parent::getHelp() .
				"<br /><span class='font-weight-bold'>Package builds are silently ignored</span>. See <a href='https://docs.apiscp.com/admin/PHP-FPM#remi-php'>Remi builds</a> in PHP-FPM.md.";
			return $this->asHtml($help);
		}
	}