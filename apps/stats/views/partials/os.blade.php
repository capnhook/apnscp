<div class="card">
	<div class="card-header" role="tab" id="headingOS">
		<h5 class="mb-0">
			<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#os" aria-expanded="true"
			   aria-controls="os">
				OS Information
			</a>
		</h5>
	</div>

	<div id="os" class="collapse show" role="tabpanel" aria-labelledby="headingOs">
		<div class="card-block">
			<table width="100%">
				<tr>
					<td>
						<h4>Canonical Hostname</h4>
					</td>
					<td>
						<?=$Page->get_canonical_hostname()?>
					</td>
				</tr>
				<tr>
					<td class="">
						<h4>Primary IP Address</h4>
					</td>
					<td>
						<?=$Page->get_primary_ip_addr()?>
					</td>
				</tr>
				<tr>
					<td class="">
						<h4>Kernel</h4>
					</td>
					<td>
						<?=$Page->get_kernel()?>
					</td>
				</tr>
				<tr>
					<td class="">
						<h4>Distro</h4>
					</td>
					<td>
						<?=$Page->get_os_release()?>
					</td>
				</tr>
				<tr>
					<td class="">
						<h4>Uptime</h4>
					</td>
					<td>
						<?=$Page->get_uptime()?>
					</td>
				</tr>
				<tr>
					<td class="">
						<h4>Load Average</h4>
					</td>
					<td>
						<?=join(',', $Page->get_load_average())?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>