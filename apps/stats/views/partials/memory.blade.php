<div class="card">
	<div class="card-header" role="tab" id="headingMemory">
		<h5 class="mb-0">
			<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#memory" aria-expanded="true"
			   aria-controls="memory">
				Memory
			</a>
		</h5>
	</div>

	<div id="memory" class="collapse" role="tabpanel" aria-labelledby="headingMemory">
		<div class="card-block p-0">
			<table width="100%" class="table">
				<thead>
				<tr>
					<th>
						Type
					</th>
					<th>
						Percent Capacity
					</th>
					<th class="right">
						Free
					</th>
					<th class="right">
						Used
					</th>
					<th class="right">
						Size
					</th>
				</tr>
				</thead>
				<?php
				$meminfo = $Page->memory_information();
				$info = $meminfo['ram'];
				?>
				<tr>
					<td>
						Physical Memory
						<span data-toggle="tooltip" class="ui-action-tooltip ui-action"
						      data-title="{{ $vars['definitions']['free-memory'] }}"></span>
					</td>
					<td>
						<?php printf("<div class='ui-gauge' style='display:block;'>
							<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
							<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
			</div>
			<span class='ui-gauge-label'>%.2f%%</span>",
							floor($info['t_used'] / $info['total'] * 100),
							ceil((1 - $info['t_used'] / $info['total']) * 100),
							$info['t_used'] / $info['total'] * 100) ?>
					</td>
					<td class="right">
						<?php printf("%.2f MB", $info['t_free'] / 1024.); ?>
					</td>
					<td class="right">
						<?php printf("%.2f MB", $info['t_used'] / 1024.) ?>
					</td>
					<td class="right">
						<?=\Formatter::commafy(sprintf("%.2f MB", ($info['total'] / 1024))); ?>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;&nbsp;- Kernel + applications
					</td>
					<td>
						<?php printf("<div class='ui-gauge' style='display:block;'>
					<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
					<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
		</div>
		<span class='ui-gauge-label'>%.2f%%</span>",
							floor($info['app'] / $info['total'] * 100),
							ceil((1 - $info['app'] / $info['total']) * 100),
							$info['app'] / $info['total'] * 100) ?>
					</td>
					<td class="right">
						&nbsp;
					</td>
					<td class="right">
						<?php printf("%.2f MB", $info['app'] / 1024.);?>
					</td>
					<td class="right">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;&nbsp;- Buffers
					</td>
					<td>
<?php printf("<div class='ui-gauge' style='display:block;'>
				<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
				<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
	</div><span class='ui-gauge-label'>%.2f%%</span>",
							floor($info['buffers'] / $info['total'] * 100),
							ceil((1 - $info['buffers'] / $info['total']) * 100),
							$info['buffers'] / $info['total'] * 100) ?>
					</td>
					<td class="right">
						&nbsp;
					</td>
					<td class="right">
						<?php printf("%.2f MB", $info['buffers'] / 1024); ?>
					</td>
					<td class="right">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;&nbsp;- Cached
					</td>
					<td>
						<?php printf("<div class='ui-gauge' style='display:block;'>
			<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
			<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
</div> <span class='ui-gauge-label'>%.2f%%</span>",
							floor($info['cached'] / $info['total'] * 100),
							ceil((1 - $info['cached'] / $info['total']) * 100),
							$info['cached'] / $info['total'] * 100) ?>

					</td>
					<td class="right">
						&nbsp;
					</td>
					<td class="right">
						<?php printf("%.2f MB", $info['cached'] / 1024); ?>
					</td>
					<td class="right">
						&nbsp;
					</td>
				</tr>
				<tr class="font-weight-bold">
					<td>
						&nbsp;&nbsp;- Available
							<span data-toggle="tooltip" class="ui-action-tooltip ui-action" data-title="{{ $vars['definitions']['available-memory'] }}"></span>
					</td>
					<td>
						<div class='ui-gauge' style='display:block;'>
							<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:{{ floor($info['available'] / $info['total'] * 100) }}%'></div>
							<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:{{ ceil((1 - $info['available'] / $info['total']) * 100) }}%'></div>
						</div>
						<span class='ui-gauge-label'>{{ sprintf("%.2f%%", $info['available'] / $info['total'] * 100)  }}</span>
					</td>
					<td class="right">
						{{ sprintf("%.2f MB", $info['available'] / 1024) }}
					</td>
					<td class="right">

					</td>
					<td class="right">
						&nbsp;
					</td>
				</tr>
				<?php
				// check if devices configured for swap, then output swap
				if ($meminfo['devswap']):
				$info = $meminfo['swap']; // handle swap
				?>
				<tr>
					<td>
						Disk Swap
					</td>
					<td>
						<?php printf("<div class='ui-gauge' style='display:block;'>
		<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
		<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
		</div> <span class='ui-gauge-label'>%.2f%%</span>",
							$info['percent'],
							(100 - $info['percent']),
							($info['total'] - $info['free']) / $info['total'] * 100) ?>
					</td>
					<td class="right">
						&nbsp;
					</td>
					<td class="right">
						<?php printf("%.2f MB", $info['used'] / 1024); ?>
					</td>
					<td class="right">
						<?=\Formatter::commafy(sprintf("%.2f MB", $info['total'] / 1024));?>
					</td>
				</tr>
				<?php
				foreach ($meminfo['devswap'] as $device => $info):
				?>
				<tr>
					<td>
						&nbsp;&nbsp;- <?=$device?>
					</td>
					<td>
						<?php printf("<div class='ui-gauge' style='display:block;'>
		<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
		<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
		</div> <span class='ui-gauge-label'>%.2f%%</span>",
							$info['percent'],
							(100 - $info['percent']),
							($info['total'] - $info['free']) / $info['total'] * 100) ?>
					</td>
					<td class="right">
						<?php printf("%.2f MB", $info['free'] / 1024); ?>
					</td>
					<td class="right">
						<?php printf("%.2f MB", $info['used'] / 1024); ?>
					</td>
					<td class="right">
						<?=\Formatter::commafy(sprintf("%.2f MB", $info['total'] / 1024)); ?>
					</td>
				</tr>
				<?php
				endforeach;
				endif;
				?>
			</table>
		</div>
	</div>
</div>