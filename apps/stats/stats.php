<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\stats;

	use Page_Container;

	class Page extends Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			$this->add_css('stats.css');
			$this->add_javascript('stats.js');

		}

		public function on_postback($params)
		{

		}

		public function getProcessors()
		{
			return $this->common_get_processor_information();
		}

		public function getPCIDevices()
		{
			return $this->common_list_pci_devices();
		}

		public function get_canonical_hostname()
		{
			return $this->common_get_canonical_hostname();
		}

		public function get_primary_ip_addr()
		{
			return $this->common_get_listening_ip_addr();
		}

		public function get_kernel()
		{
			return $this->common_get_kernel_version();
		}

		public function get_os_release()
		{
			return $this->common_get_operating_system();
		}

		public function get_uptime()
		{
			return $this->common_get_uptime();
		}

		public function get_load_average()
		{
			return $this->common_get_load();
		}

		public function network_information()
		{
			return $this->stats_get_network_device_information();
		}

		public function memory_information()
		{
			return $this->stats_get_memory_information();
		}

		public function filesystem_information()
		{
			return $this->stats_get_partition_information();
		}

		public function spamassassin()
		{
			return $this->stats_get_spamassassin_stats();
		}
	}

?>
