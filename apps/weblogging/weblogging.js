$(document).ready(function () {
	$('#domain').change(function () {
		var $s, $col = $([]), $group,
			domain = $(this).val(),
			domainlen = (logs[domain].length);

		$('#subdomains').empty();

		$.map($.merge(logs[domain], logs['*']), function (n, i) {
			var label = n;
			if (n == 'www') n = '';
			if (i == 0) {
				$group = $('<optgroup label="Local"></optgroup>');
			} else if (i == domainlen) {
				$('#subdomains').append($group);
				$group = $('<optgroup label="Global"></optgroup>');
			}
			$group.append('<option value="' + n + '">' + label + '</option>');
		});
		$('#subdomains').append($group);
	});
});

$(window).on('load', function () {
	$('#domain').change();
});
