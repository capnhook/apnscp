$(document).ready(function () {
	var tmp = $('#bandwidth-span').val(),
		start = tmp.substr(0, tmp.indexOf(",")),
		end = tmp.substr(tmp.indexOf(",") + 1);
	getBWData(start, end);
});

function getBWData(start, end) {
	apnscp.call_app('bandwidthbd', 'get_bandwidth', [start, end], {dataType: "json"}).then(function (data) {
		if (data['*']) {
			/**
			 * doesn't this fuck with v8 hidden classes?
			 **/
			delete data['*'];
		}
		if (data.length < 1) {
			return false;
		}
		var datapoints = [];
		for (i in data) {
			var items = data[i]['items'];
			for (j in items) {
				var item = items[j],
					label = item['svc_name'],
					sum = item['in'] + item['out'],
					color = item['color'];
				if (item['ext_info']) {
					label += ' (' + item['ext_info'] + ')';
				}
				datapoints.push({label: label, data: sum, color: color});
			}
		}
		$.plot('#bwgraph', datapoints, {
			series: {
				pie: {
					clickable: true,
					hoverable: true,
					height: '100px',
					show: true,
					radius: 1,
					combine: {
						threshold: 0.03
					},
					label: {
						background: {opacity: 0.5, color: '#000'},
						show: true,
						radius: 3 / 4,
						formatter: function (label, series) {
							return '<span class="graph-label">' + Math.round(series.percent) + '%</span>';
						}
					},
					stroke: {
						width: 0
					}
				}
			},
			legend: {
				show: true,
				margin: 0,
				container: $('#bwlegend'),
				labelFormatter: function (name, series) {
					return '<span>' + name + ' &ndash; ' + Math.round(series.percent) + '%</span>';
				}
			}
		});
	});
}