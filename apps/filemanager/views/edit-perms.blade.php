@include('partials.manager-header')
@php
	$contents = $Page->getPerms();
@endphp
@if (!$Page->errors_exist())
@php
	$file = $_GET['ep'];
	$stats = $Page->statFile($file);
@endphp
<tr>
	<td class="" colspan="6">
		<h4>File Information</h4>
	</td>
</tr>
<tr>
	<td class="nospacing" colspan="6" class="cell1">
		<table width="100%" cellpadding="2" class=" table-borderless" cellspacing="1">
			<tr>
				<td width="15%" class="odd">
					<strong>Owner</strong>
				</td>
				<td width="35%" class="odd">
					<?=$stats['owner']?>
				</td>
				<td width="15%" class="odd">
					<strong>Group</strong>
				</td>
				<td width="35%" class="odd">
					<?=$stats['group']?>
				</td>
			</tr>
			<tr>
				<td class="even">
					<strong>Created On</strong>
				</td>
				<td class="even">
					<?=date('r', $stats['ctime'])?>
				</td>
				<td class="even">
					<strong>Last Modified</strong>
				</td>
				<td class="even">
					<?=date('r', $stats['mtime'])?>
				</td>

			</tr>
			<tr>
				<td class="odd">
					<strong>Accessed On</strong>
				</td>
				<td class="odd">
					<?=date('r', $stats['atime'])?>
				</td>
				<td class="odd">
					<strong>Size</strong>
				</td>
				<td class="odd">
					<?=\Formatter::reduceBytes($stats['size'])?>
				</td>
			</tr>
		</table>
	</td>
</tr>
	<tr>
		<td colspan="6" class="nospacing">
			<table width="100%" cellspacing="0" class="" cellpadding="2" id="permission-table">
				<tr>
					<td colspan="5" class="">
						<h4>Manage Access Control List</h4>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<ul class="acl-manage">
							<?php

							?>
						</ul>
						<ul class="acl-active">

						</ul>
						<?php
						$count = 0;
						foreach ($Page->getACLs($_GET['ep']) as $acl):
							if (isset($acl['USER']) || isset($acl['GROUP']))
								continue;
							$permStr = '';
							if (($acl['permissions'] & 4) == 4)
								$permStr .= 'r';
							else
								$permStr .= '-';

							if (($acl['permissions'] & 2) == 2)
								$permStr .= 'w';
							else
								$permStr .= '-';

							if (($acl['permissions'] & 1) == 1)
								$permStr .= 'x';
							else
								$permStr .= '-';
							if (isset($acl['user']))
								print ('user: ' . $acl['user'] . ' => ' . $permStr . "<br />");
							else if (isset($acl['group']))
								print ('group: ' . $acl['group'] . ' => ' . $permStr . "<br />");
							$count++;
						endforeach;
						if (!$count)
							print "None Set";
						?>
					</td>
				</tr>
				<tr>
					<td colspan="5" class="">
						<h4>Change Permissions</h4>
					</td>
				</tr>
				<?php
				$Page->determinePerms($stats['permissions']);
				if ($stats['can_chown'] && !$stats['link']):
				?>

				<tr>
					<td class="cell1" width="15%"><strong>Owner</strong></td>
					<td class="cell1" width="15%"><strong>Group</strong></td>
					<td class="cell1" width="15%"><strong>Other</strong></td>
					<td class="cell1" width="20%"><strong>User</strong></td>
					<td class="cell1"><strong>Misc Help</strong></td>
				</tr>
				<tr>
					<td class="cell1" vAlign="top">
						<label class="custom-control custom-checkbox mb-0" for="owner_read">
							<input id="owner_read" type="checkbox" name="permission[]" class="custom-control-input"
							       value="0x0100" {{ $Page->printPermissionSet($stats, 'read', 'user') }} />
							<span class="custom-control-indicator"></span>
							Read
						</label>

						<label class="custom-control custom-checkbox mb-0" for="owner_write">
							<input id="owner_write" type="checkbox" name="permission[]" class="custom-control-input"
							       value="0x0080" {{ $Page->printPermissionSet($stats, 'write', 'user') }} />
							<span class="custom-control-indicator"></span>
							Write
						</label>
						<label class="custom-control custom-checkbox mb-0" for="owner_execute">
							<input id="owner_execute" type="checkbox" name="permission[]" class="custom-control-input"
							       value="0x0040" {{ $Page->printPermissionSet($stats, 'execute', 'user') }} />
							<span class="custom-control-indicator"></span>
							Execute
						</label>
					</td>
					<td class="cell1" vAlign="top">
						<label class="custom-control custom-checkbox mb-0" for="group_read">
							<input id="group_read" type="checkbox" name="permission[]" class="custom-control-input"
							       value="0x0020" {{ $Page->printPermissionSet($stats, 'read', 'group') }} />
							<span class="custom-control-indicator"></span>
							Read
						</label>

						<label class="custom-control custom-checkbox mb-0" for="group_write">
							<input id="group_write" type="checkbox" name="permission[]" class="custom-control-input"
							       value="0x0010" {{ $Page->printPermissionSet($stats, 'write', 'group') }} />
							<span class="custom-control-indicator"></span>
							Write
						</label>
						<label class="custom-control custom-checkbox mb-0" for="group_execute">
							<input id="group_execute" type="checkbox" name="permission[]" class="custom-control-input"
							       value="0x0008" {{ $Page->printPermissionSet($stats, 'execute', 'group') }} />
							<span class="custom-control-indicator"></span>
							Execute
						</label>
					</td>
					<td class="cell1" vAlign="top">
						<label class="custom-control custom-checkbox mb-0" for="other_read">
							<input id="other_read" type="checkbox" name="permission[]" class="custom-control-input"
							       value="0x0004" {{ $Page->printPermissionSet($stats, 'read', 'other') }} />
							<span class="custom-control-indicator"></span>
							Read
						</label>

						<label class="custom-control custom-checkbox mb-0" for="other_write">
							<input id="other_write" type="checkbox" name="permission[]" class="custom-control-input"
							       value="0x0002" {{ $Page->printPermissionSet($stats, 'write', 'other') }} />
							<span class="custom-control-indicator"></span>
							Write
						</label>
						<label class="custom-control custom-checkbox mb-0" for="other_execute">
							<input id="other_execute" type="checkbox" name="permission[]" class="custom-control-input"
							       value="0x0001" {{ $Page->printPermissionSet($stats, 'execute', 'other') }} />
							<span class="custom-control-indicator"></span>
							Execute
						</label>
					</td>

					<td class="align-top" rowspan="3">
						<select class="form-control custom-select" name="user_ownership">
							@foreach ($Page->list_users() as $user)
								<option name='{{ $user }}'
								        @if ($user == $stats['owner']) SELECTED @endif>{{ $user }}</option>
							@endforeach
						</select>
						<label class="custom-control custom-checkbox mb-0" for="chown_recursive">
							<input type="checkbox" name="chown_recursive" id="chown_recursive"
							       value="" class="custom-control-input"/>
							<span class="custom-control-indicator"></span>
							apply recursively
						</label>
					</td>
					<input type="hidden" name="file" value="<?= $_GET['ep'] ?>"/>
					<td class="cell1 align-top" rowspan="<?php print($stats['file_type'] == 'dir' ? 3 : 2);?>">
						<ul class="help-section">
							<li/>
							CGI scripts require the <a class="ui-action ui-action-label ui-action-kb"
							                           href="<?=MISC_KB_BASE?>/cgi-passenger/cgi-fastcgi-permissions/">exact
								permission</a> set <em>755</em>
							<li/>
							Confused about permissions? <a class="ui-action ui-action-label ui-action-kb"
							                               href="<?=MISC_KB_BASE?>/guides/permissions-overview/">Permission
								Guide</a>
							<li/>
							ACLs allow granular, user control over files
						</ul>
					</td>
				</tr>
				@if ($stats['file_type'] == 'dir')
					<tr>
						<td colspan="3" class="cell1">
							<div class="odd center py-1 octal-summary">
								<strong>Octal Value</strong>
								<span id="permission_view">
								{{ sprintf("%o", $stats['permissions'] & ~octdec(770000)) }}
	                        </span>
								<label class="custom-control custom-checkbox mb-0 ml-3" for="recursive">
									<input type="checkbox" name="recursive" id="recursive"
									       value="1" class="custom-control-input"/>
									<span class="custom-control-indicator"></span>
									apply recursively
								</label>

							</div>

						</td>
					</tr>
				@endif
				<tr>
					<td colspan="5" class="cell1" vAlign="top">
						<button type="submit" value="Change Permissions" class="btn btn-primary ml-0 mr-2 my-2"
						        name="Change_Permissions">
							Change Permissions
						</button>
						<input type="hidden" name="original_permissions"
						       value="<?=(sprintf("%o", $stats['permissions'] & ~octdec(770000)))?>"/>
						<button class="btn btn-secondary warn m-2" type="reset" value="Reset"
						        OnClick="$('#permission_view').text('<?=sprintf("%o",
									$stats['permissions'] & ~octdec(770000))?>'); return true;">
							Reset
						</button>

						<?php
						if ($stats['can_write'] && $stats['can_read'] && $stats['file_type'] == 'file'):
						$props = $Page->getProperties($file);
						$mime = $props['mime'];
						$eol = 'unix';
						/**
						 * Initially preferred a JS approach, but Chrome interprets
						 * data in a textarea as \n even if the spec calls for \r\n
						 */
						if (false !== strpos($mime, ' CRLF ')) {
							$eol = 'windows';
						} else if (false !== strpos($mime, ' CR ')) {
							$eol = 'mac';
						}
						?>
						<div class="btn-group text-right hidden-sm-down eol-container dropup">
							<button type="button" class="btn btn-secondary dropdown-toggle m-2" data-toggle="dropdown"
							        aria-haspopup="true" aria-expanded="false">
								Set EOL
								<span class="marker"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu dropdown-menu-right eol-types">
								<label class="windows dropdown-item">
									<button class="dropdown-item" type="submit" name="eol" value="windows">
										Win (\r\n)
									</button>
								</label>
								<label class="unix dropdown-item">
									<button class="dropdown-item" type="submit" name="eol" value="unix">
										Unix (\n)
									</button>
								</label>
								<label class="mac dropdown-item">
									<button class="dropdown-item" type="submit" name="eol" value="mac">
										Mac (\r)
									</button>
								</label>
								<div class="dropdown-divider"></div>
								<small class=" dropdown-item">Detected <span id="EOL"><?=ucwords($eol)?></span></small>
								<input type="hidden" name="original-eol" id="eol-original" value="<? print $eol; ?>"/>
							</div>
						</div>
					<?php endif; ?>
				</tr>
				<?php
				else:
				?>
				<tr>
					<td class="cell1" colspan="4" align="center">Cannot Modify Permissions</td>
					<td class="cell1">&nbsp;</td>
				</tr>
				<?php
				endif;
				?>

			</table>
		</td>
	</tr>
@endif