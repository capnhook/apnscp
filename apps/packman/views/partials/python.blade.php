<p>
	Python packages may be installed from
	<a href="https://pypi.python.org/pypi" class="ui-action ui-action-label ui-action-visit-site">PyPI</a>
	using pip.
	@if (version_compare(platform_version(), '6', '>='))
		Optionally change your Python version using
		<a class="ui-action ui-action-kb ui-action-label" href="<?=MISC_KB_BASE?>/python/changing-python-versions/">pyenv</a>
		before running pip.
	@endif
</p>
@if (version_compare(platform_version(), '7.5', '>='))
	<div class="mb-3">
		<p class="example">
			Example: to install Python versions
		</p>
		<pre class="command-line" data-host="{{ SERVER_NAME_SHORT }}"
		     data-user="{{ \Session::get('username', 'root') }}">
		<code class="language-bash">pyenv install 3.4.1
		# Switch to 3.4.1 locally
		pyenv local 3.4.1
		# Set 3.4.1 as the default system interpreter for future logins
		pyenv global 3.4.1
		</code>
	</pre>
	</div>
@endif

<p class="example">
	Example: to install <a href="<?=MISC_KB_BASE?>/python/django-quickstart/"
	                       class="ui-action ui-action-label ui-action-kb">Django</a>,
</p>
<pre class="command-line" data-host="{{ SERVER_NAME_SHORT }}" data-user="{{ \Session::get('username', 'root') }}">
    <code class="language-bash">pip install django</code>
</pre>