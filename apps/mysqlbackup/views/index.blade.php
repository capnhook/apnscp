<form method="post">
	<table width="100%" class="table">
		<thead>
		<tr>
			<th class="">
				Database Name
			</th>
			<th class="hidden-sm-down">
				Extension
			</th>
			<th class="hidden-sm-down">
				Frequency (days)
			</th>
			<th class="">
				Limit
			</th>
			<th class="">
				Next Backup
			</th>
			<th class="hidden-sm-down">
				E-mail
			</th>
			<th class="center">
				Actions
			</th>
		</tr>
		</thead>
		@foreach ($Page->list_backups() as $db_name => $backup_params)
			@include('partials.backup-entry')
		@endforeach
	</table>

	<h3>Add Task</h3>
	@includeWhen(!($dbs = $Page->get_dbs()), 'partials.no-databases')
	@includeWhen($dbs, 'partials.add-task', $dbs)
</form>
