<hr/>
<fieldset class="form-group my-3 row">
	<h5 class="col-12 form-control-label">
		<i class="fa fa-envelope"></i>
		Email
	</h5>
	<div class="col-sm-6">
		<div class="d-flex align-items-center">
			<label class="pl-0 mb-0 form-inline custom-checkbox custom-control align-items-center d-inline-flex">
				<input type="checkbox" class="custom-control-input" name="email_enable" id="email_enable"
				       value="1" {{ $Page->get_option('email_enable', 'checkbox') }} />
				<span class="custom-control-indicator"></span>
				Enable
			</label>

			<button name="Advanced" type="button" id="email_advanced" value="Advanced" class="btn btn-secondary"
			        data-toggle="collapse"
			        data-target="#emailOptions" aria-expanded="false" aria-controls="emailOptions">
				<i class="ui-action-advanced ui-action"></i>
				Advanced
			</button>
		</div>

		<div id="emailOptions" class="collapse mt-2">
			<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
				<input type="checkbox" class="custom-control-input" name="email_smtp" value="1"
				       id="email_smtp" {{ $Page->get_option('email_smtp', 'checkbox') }} />
				<span class="custom-control-indicator"></span>
				User may send email through server
			</label>

			<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
				<input type="checkbox" class="custom-control-input" name="email_imap" value="1"
				       id="email_imap" {{ $Page->get_option('email_imap', 'checkbox') }} />
				<span class="custom-control-indicator"></span>
				User may login to IMAP/POP3 server
			</label>

			@if ($Page->get_mode() == 'add')
				<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
					<input type="checkbox" class="custom-control-input" name="email_create" value="1"
					       id="email_create" {{ $Page->get_option('email_create', 'checkbox') }} />
					<span class="custom-control-indicator"></span>
					Create email addresses named after user
				</label>
			@endif

			<label>
				Create e-mail addresses for the selected domains:
			</label>
			<select name="email_domains[]" class="custom-select form-control" id="email_domains" multiple="multiple">
				@foreach (\Util_Conf::mailbox_domains() as $domain)
					<option value="{{ $domain }}" {{ $Page->get_option('email_domains', 'option', $domain) }}
					>{{ $domain }}</option>
				@endforeach
			</select>
		</div>
	</div>
</fieldset>