<hr/>
<fieldset class="form-group my-3 row">
	<h5 class="col-12 form-control-label">
		<i class="fa fa-upload"></i>
		FTP
	</h5>
	<div class="col-md-8">
		<div class="align-items-center d-flex">
			<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-inline-flex mb-0">
				<input type="checkbox" class="custom-control-input" name="ftp_enable" id="ftp_enable"
				       value="1" {{ $Page->get_option('ftp_enable', 'checkbox') }} />
				<span class="custom-control-indicator"></span>
				Enable
			</label>
			@if ($Page->get_mode() !== 'defaults')
				<button type="button" name="Advanced" id="ftp_advanced" value="Advanced" class="btn btn-secondary"
				        data-toggle="collapse"
				        data-target="#ftpOptions" aria-expanded="false" aria-controls="ftpOptions">
					<i class="ui-action-advanced ui-action"></i>
					Advanced
				</button>
			@endif

			<div class="inline-block" data-toggle="tooltip" title="Users may not access folders outside of the jail">
				<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex mb-0 ml-3">
					<input type="checkbox" class="custom-control-input" name="ftp_jail" id="ftp_jail"
					       value="1" {{ $Page->get_option('ftp_jail', 'checkbox') }} />
					<span class="custom-control-indicator"></span>
					Jail to home directory
				</label>
			</div>
		</div>

		<div class="row ">
			<div id="ftpOptions" class="col-12 mt-2 @if ($Page->get_mode() !== 'defaults') collapse @endif @if ($Page->get_option('ftp_jail_path')) show @endif">
				<label>Custom Home Directory</label>
				<div class="row">
					<div class="col-xl-6 col-12 input-group">
						<input type="text" class="form-control" placeholder="Select a path" name="ftp_jail_path"
						       value="{{ $Page->get_option('ftp_jail_path') }}" id="custom_jail"
						       class="dir_browser"/>
						<div class="btn-group">
							<button type="button" id="jail_chpath" class="btn btn-secondary" name="jail_chpath">
								<i class="fa fa-folder-open"></i>
								Browse
							</button>
						</div>
					</div>
				</div>
				<span class="text-muted">
                    <i class="fa fa-sticky-note-o"></i>
					Will be owned by the user, leave blank for default
                </span>

				@if ($Page->get_mode() === 'defaults')
					<div class="note mt-1">
						The following variables may be used for setting a home default. These are substituted on user
						creation.
						<dl class="row mt-1">
							<dt class="col-1 mb-0">%u</dt>
							<dd class="col-11 mb-0">Username</dd>
							<dt class="col-1 mb-0">~</dt>
							<dd class="col-11 mb-0">User's home directory, <em>/home/%u</em></dd>
						</dl>
						<em>Example:</em> <code>~/public_html</code> &ndash; jails user to public_html/ directory.
					</div>

				@endif
			</div>
		</div>
	</div>
</fieldset>