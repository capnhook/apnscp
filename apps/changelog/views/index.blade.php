<p>
	Recent changes from apnscp
	<strong>v{{ sprintf("%d.%d.%s", $cp_rev['ver_maj'], $cp_rev['ver_min'], $cp_rev['ver_patch']) }}
		{{ substr($cp_rev['revision'],0, 8) }} ({{ date('Ymd', $cp_rev['timestamp']) }})
	</strong>:
</p>
<ul class="list-unstyled">
	@foreach($changelog as $commit)
		@unless (preg_match_all('/^\s*(Fixed|FIX|NEW|ADD|Added|Removed|REM|Changed|CHG):(.+)$/i',$commit['msg'],$capture))
			@continue
		@endunless
		<li class="mb-3">
			<small>
				<i class="fa fa-git-alt"></i> Rev: {{ substr($commit['commit'], 0, 8) }}
			</small>
			<br/>
			<small>
				<i class="fa fa-clock-o"></i> Date: {{ date('r', $commit['ts']) }}
			</small>
			<p>
				{!! nl2br(e($commit['msg'])) !!}
			</p>
		</li>
	@endforeach
</ul>