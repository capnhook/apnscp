---
home: true
tagline: Managing an ApisCP plaform 
footer: Copyright © 2020 Apis Networks
---

# Introduction

apnscp is an modern hosting panel + platform that began in 2002 as a panel for Apis Networks. apnscp runs agentless and is 100% self-hosted. Licenses can be purchased from [my.apnscp.com](https://my.apnscp.com).

## Requirements

-   2 GB RAM
-   [Forward-confirmed reverse DNS](https://en.wikipedia.org/wiki/Forward-confirmed_reverse_DNS), i.e. 64.22.68.1 <-> apnscp.com
-   CentOS 7.x or RedHat 7.x

## Installation

See [docs.apnscp.com](https://docs.apnscp.com/getting-started/installation/). Installation may be customized using the [utility](https://apnscp.com/#customize) on apnscp.com.

## Contributing

Use the [Issue Tracker](https://bitbucket.org/apisnetworks/apnscp/issues?status=new&status=open) to post feature requests.

## License

apnscp is (c) Apis Networks. All components except for third-party modules and [apnscp modules](https://github.com/apisnetworks/apnscp-modules) are licensed under a [commercial license](https://bitbucket.org/apisnetworks/apnscp/raw/HEAD/LICENSE). Contact licensing@apisnetworks.com for licensing enquiries. Any dissemination of material herein is prohibited without expressed written consent of Apis Networks.

